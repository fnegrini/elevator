#ifndef CORE_OUTPUTTYPE_H
#define CORE_OUTPUTTYPE_H

#include <list>

namespace Core
{
	enum OutputType
	{
		OT_STREAM,
		OT_WINDOW,
	};

}

#endif
