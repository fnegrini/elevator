#include "InputPersonBase.h"
#include "FactoryBase.h"

#include <fstream>

namespace Core
{

const char REQUEST_FILE[] = "StatRequests.csv";

InputPersonBase::InputPersonBase(const char *sName, LogBase *oLogBase, ControlerBase *oControlerBase, FactoryBase *oFactory)
:ComponentBase(sName, oLogBase)
{

	MyControler = oControlerBase;

	MyFactory = oFactory;


}

Person * InputPersonBase::CreatePerson(const char * PersonName, InputRequestList *Requests)
{
	Person *Aux;
	InputRequest *RequestAux;
	InputRequestListIterator it;

	Aux = new Person(PersonName, GetLogBase(), MyControler, MyFactory);

	MyPersonList.push_back(Aux);

	for (it = Requests->begin(); it != Requests->end(); it++) {
		
		RequestAux = *it;

		Aux->CreateRequest(RequestAux->GetFloorFrom(), RequestAux->GetFloorTo(), RequestAux->GetCyclesBeforeStart());
	}

	return Aux;

}

void InputPersonBase::LogStatistics()
{
	PersonListIterator it;
	Person *PersonAux;
	string msg;

	ofstream File;

	File.open(REQUEST_FILE, ios::out | ios::trunc);

	// Create header file
	File << "PERSON,FLOOR FROM,FLOOR TO,STATUS,WAITING OUT,WAITING IN\n";

	for (it = MyPersonList.begin(); it != MyPersonList.end(); it++) {

		PersonAux = *it;

		PersonAux->LogStatistics(&File);

	}

	File.close();

	msg = string("Statistics for requests logged in file '") + string(REQUEST_FILE) + string("'");

	LogMessageInternal(MSG_NOTIFICATION, msg.c_str());

}

void InputPersonBase::SetControler(ControlerBase * oControler)
{
	MyControler = oControler;
}



void InputPersonBase::Run()
{
	PersonListIterator it;
	Person *PersonAux;
	
	for (it = MyPersonList.begin(); it != MyPersonList.end(); it++) {

		PersonAux = *it;

		// Start idle requests
		if (PersonAux->GetState() == PS_IDLE) {

			PersonAux->StartComponent();
		}

		// Stop finished requests
		if (PersonAux->GetState() == PS_DONE) {

			PersonAux->StopComponent();
		}
	}
}

PersonList * InputPersonBase::GetPersonList()
{
	return &MyPersonList;
}

}