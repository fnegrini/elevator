#ifndef CORE_COMPONENT_CYCLE_THREAD_H
#define CORE_COMPONENT_CYCLE_THREAD_H

#include <string>
#include "ComponentBase.h"
#include "ComponentBaseCycleState.h"


namespace Core
{
class ComponentBaseCycleThread: public ComponentBase
{
private:

	int CycleCount;

	ComponentCycleState state;

protected:

	virtual void WaitCyclesAssyncron(int iCycleCount);

	virtual void DoAfterCycleCountEnds();

	virtual void SetState(ComponentCycleState eState);

	// Thread overrides!

	virtual void Run();

public:

	ComponentBaseCycleThread(const char *sName, LogBase *oLogBase);

};

}  // namespace Core
#endif
