#ifndef CORE_LOG_BASE_H
#define CORE_LOG_BASE_H

#include "MessageType.h"
#include <string> 
  using std::string;
#include <iostream> 
  using namespace std; 

#include <list>

namespace Core
{
class LoggerBase;


class LogBase
{
private:
	MessageTypeList activeTypes;

protected:
	
	virtual std::string MessageTypeToString(MessageType Type);

	virtual void LogMessageInternal(LoggerBase *oLoggerBase, MessageType Type, const char *Message);

	virtual bool MessageTypeActive(MessageType eType);

	virtual std::string GetTimeStamp();

public:
	LogBase();

	void LogMessage(LoggerBase *oLoggerBase, MessageType eType, const char *sMessage);

	MessageTypeList GetActiveTypes();

	void EnableMessageType(MessageType eType);

	void DisableMessageType(MessageType eType);

};

}  // namespace Core
#endif
