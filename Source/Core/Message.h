#ifndef CORE_MESSAGE_H
#define CORE_MESSAGE_H

#include <string>
#include "MessageType.h"

namespace Core
{

class ComponentBase;

class Message
{
private:
	ComponentBase *component;
	MessageType type;
	std::string msg;

public:
	Message(ComponentBase *oComponent=NULL, MessageType eType=MSG_UNDEFINED, std::string oMessage=NULL);

	const ComponentBase *GetComponent();
	void SetComponent(ComponentBase *oComponent);

	MessageType GetMessageType();
	void SetMessageType(MessageType eType);

	const std::string GetMessage();
    void SetMessage(std::string oMessage);

};

}

#endif