#include "Message.h"


Core::Message::Message(ComponentBase *oComponent, MessageType eType, std::string oMessage)
{
	component = oComponent;
	type = eType;
	msg = oMessage;
}

const Core::ComponentBase * Core::Message::GetComponent()
{
	return component;
}

void Core::Message::SetComponent(ComponentBase *oComponent)
{
	component = oComponent;
}

Core::MessageType Core::Message::GetMessageType()
{
	return type;
}

void Core::Message::SetMessageType(MessageType eType)
{
	type = eType;
}

const std::string Core::Message::GetMessage()
{
	return msg;
}

void Core::Message::SetMessage(std::string oMessage)
{
	msg = oMessage;
}

