#ifndef CORE_ELEVATOR_CONT_ABS_INTERFACE_H
#define CORE_ELEVATOR_CONT_ABS_INTERFACE_H

namespace Core {

class ElevatorControler;

class Trip;

class ElevatorControlerAbstractInterface
{
private:
	
	ElevatorControler *MyElevatorControler;

protected:

	ElevatorControler *GetElevatorControler();

public:

	ElevatorControlerAbstractInterface(ElevatorControler *oElevatorControler = nullptr);

	~ElevatorControlerAbstractInterface();

	virtual void TripStarted(Trip *oTrip) abstract;

	virtual void RequestCommited(Trip *oTrip, int Floor) abstract;

	virtual void TripFinished(Trip *oTrip) abstract;
};

}
#endif