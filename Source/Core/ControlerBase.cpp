
#include "ControlerBase.h"
#include "FactoryBase.h"
#include "../Controler/ControlerCUpCDown.h"
#include "../Controler/ControlerCUpSDown.h"
#include "../Controler/ControlerSUpCDown.h"
#include "../Controler/ControlerSUpSDown.h"

#include <fstream>

namespace Core
{

const char ELEVATOR_FILE[] = "StatElevators.csv";

Trip * ControlerBase::GetIdleTrip(Request * oRequest)
{
	ElevatorControlerIterator *it;
	ElevatorControler *oElevatorControler;
	Trip *oTrip;
	TripDirection eDirection;

	eDirection = oRequest->GetDirection();

	// 1st Try: Check if there is an idle trip to same direction
	it = MyControlerList.GetFirst();

	while (it != nullptr) {

		oElevatorControler = it->GetElement();

		oTrip = oElevatorControler->GetIdleTrip(eDirection);

		if (oTrip != nullptr) {

			return oTrip;
		}

		it = it->GetNext();
	}

	return nullptr;
}

Trip * ControlerBase::GetIdleElevator(Request * oRequest)
{
	ElevatorControlerIterator *it;
	ElevatorControler *oElevatorControler;
	Trip *oTrip;
	TripDirection eDirection;
	ElevatorControlerAction eAction;

	eDirection = oRequest->GetDirection();

	it = MyControlerList.GetFirst();

	while (it != nullptr) {

		oElevatorControler = it->GetElement();

		eAction = oElevatorControler->GetAction();

		if (eAction == ECA_IDLE) {

			oTrip = oElevatorControler->CreateTrip(eDirection);

			return oTrip;
		}

		it = it->GetNext();
	}

	return nullptr;
}

Trip * ControlerBase::GetOccupedElevatorBalanced(Request * oRequest)
{
	ElevatorControlerIterator *it = nullptr;
	ElevatorControler *oElevator = nullptr;
	Trip *oTrip = nullptr;
	int BalancedIndex;
	int i;

	BalancedIndex = (BalanceControl % MyControlerList.GetLength());

	it = MyControlerList.GetFirst();
	for (i = 0; i < BalancedIndex; i++) {

		if (it != nullptr) {
			
			it = it->GetNext();
		}

	}

	if (it != nullptr) {

		oElevator = it->GetElement();
	
		oTrip = oElevator->CreateTrip(oRequest->GetDirection());

	}

	return oTrip;
}

Trip *ControlerBase::DetermineControlerToRequest(Request * oRequest)
{
	Trip *oTrip;

	// 1st Try: Check if there is an idle trip to same direction
	oTrip = GetIdleTrip(oRequest);

	// 2nd Try: Check if there is an idle elevator (create a trip)
	if (oTrip == nullptr) {

		oTrip = GetIdleElevator(oRequest);
	}

	// 3rd Try: Append a new Trip to an elevator to a running elevator (create a trip)
	if (oTrip == nullptr) {

		oTrip = GetOccupedElevatorBalanced(oRequest);
	}

	if (oTrip != nullptr) {

		oTrip->InsertRequest(oRequest);

		if (oTrip->GetState() == TS_BLOCKED)
			oTrip->ReleaseTrip();

		return oTrip;

	}
	else {
		return nullptr;
	}

	
}

ControlerBase *ControlerBase::FactoryMethod(ControlerType oType, const char *sName, LogBase *oLogBase, FactoryBase *oFactory)
{
	switch (oType)
	{
	case Core::CT_STANDARD:
		return new ControlerBase(sName, oLogBase, oFactory);
		break;
	case Core::CT_CUP_CDOWN:
		return new Controler::ControlerCUpCDown(sName, oLogBase, oFactory);
		break;
	case Core::CT_CUP_SDOWN:
		return new Controler::ControlerCUpSDown(sName, oLogBase, oFactory);
		break;
	case Core::CT_SUP_CDOWN:
		return new Controler::ControlerSUpCDown(sName, oLogBase, oFactory);
		break;
	case Core::CT_SUP_SDOWN:
		return new Controler::ControlerSUpSDown(sName, oLogBase, oFactory);
		break;
	default:
		break;
	}

	return nullptr;
}

ControlerBase::ControlerBase(const char * sName, LogBase * oLogBase, FactoryBase *oFactory)
:LoggerBase(sName, oLogBase)
{
	MyFactory = oFactory;
	BalanceControl = 0;
}

ControlerBase::~ControlerBase()
{

}

ElevatorControler * ControlerBase::CreateControler(const char * sName, const char * sElevatorName, int iMinFloor, int iMaxFloor, int iCyclesPerFloor, int iCyclesPerDoor, unsigned int iCapacity)
{
	ElevatorControler *aux;

	aux = new ElevatorControler(sName, sElevatorName, GetLogBase(), iMinFloor, iMaxFloor, iCyclesPerFloor, iCyclesPerDoor, iCapacity, MyFactory);
	
	MyControlerList.push_back(aux);
	
	return aux;
}

ElevatorControler * ControlerBase::InsertRequest(Request * oRequest)
{
	Trip *TripAux;

	TripAux = DetermineControlerToRequest(oRequest);

	if (TripAux != nullptr)
		return TripAux->GetControler();
	else
		return nullptr;
}

void ControlerBase::StartComponent()
{
	ElevatorControlerIterator *it;
	ElevatorControler *oElevator;

	it = MyControlerList.GetFirst();

	while (it != nullptr) {

		oElevator = it->GetElement();

		oElevator->StartComponent();

		it = it->GetNext();
	}


}

void ControlerBase::StopComponent()
{
	ElevatorControlerIterator *it;
	ElevatorControler *oElevator;

	it = MyControlerList.GetFirst();

	while (it != nullptr) {

		oElevator = it->GetElement();

		oElevator->StopComponent();

		it = it->GetNext();
	}

}

void ControlerBase::LogStatistics()
{
	ElevatorControlerIterator *it;
	ElevatorControler *oControler;
	Elevator *oElevator;
	string msg;
	ofstream File;

	it = MyControlerList.GetFirst();

	File.open(ELEVATOR_FILE, ios::out | ios::trunc);

	// Create header file
	File << "ELEVATOR,CYCLES DOWN,CYCLES UP\n";

	while (it != nullptr) {

		oControler = it->GetElement();

		oElevator = oControler->GetElevator();

		msg = string("Elevator ") + string(oElevator->GetName()) + " - " +
			string(to_string(oElevator->GetTotalCyclesDown())) + string(" cycles moving down and ") +
			string(to_string(oElevator->GetTotalCyclesUp())) + string(" cycles moving up");

		LogMessageInternal(MSG_NOTIFICATION, msg.c_str());

		msg = string(oElevator->GetName()) + "," +
			string(to_string(oElevator->GetTotalCyclesDown())) + string(",") +
			string(to_string(oElevator->GetTotalCyclesUp())) + string("\n");

		File << msg.c_str();

		it = it->GetNext();
	}

	File.close();

	msg = string("Statistics for elevators logged in file '") + string(ELEVATOR_FILE) + string("'");

	LogMessageInternal(MSG_NOTIFICATION, msg.c_str());
}




}  // namespace Core
