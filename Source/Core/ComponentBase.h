#ifndef CORE_COMPONENT_BASE_H
#define CORE_COMPONENT_BASE_H

#include <string>
#include "Thread.h"
#include "LoggerBase.h"

namespace Core
{
class ComponentBase: public Thread, public LoggerBase
{
private:

	static long CycleInterval;

protected:

	virtual void PrintErrorInternal(const std::string msg);

public:
	ComponentBase(const char *sName="", LogBase *oLogBase=nullptr);

	~ComponentBase();

	static void SetCycleInterval(long sCycleInterval);

	static long  GetCycleInterval();

	virtual void StartComponent();

	virtual void StopComponent();

	virtual void LogMessageInThread(MessageType Type, const char *Message);

	virtual void WaitUpToOneCycle();

};

}  // namespace Core
#endif
