#ifndef CORE_ELEVATOR_ABS_INTERFACE_H
#define CORE_ELEVATOR_ABS_INTERFACE_H

namespace Core {

class Elevator;

class Person;

class ElevatorAbstractInterface
{
private:
	
	Elevator *MyElevator;

protected:

	Elevator *GetElevator();

public:
	
	ElevatorAbstractInterface(Elevator *oElevator = nullptr);
	
	~ElevatorAbstractInterface();

	virtual void MovingUp() abstract;

	virtual void MovingDown() abstract;

	virtual void ArrivedToFloor(int Floor) abstract;

	virtual void DoorOpened() abstract;

	virtual void DoorClosed() abstract;

	virtual void AddPerson(Person *oPerson) abstract;
};

}

#endif