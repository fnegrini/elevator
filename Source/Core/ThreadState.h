#ifndef CORE_THREADSTATE_H
#define CORE_THREADSTATE_H

#include <list>

namespace Core
{
	enum Threadstate
	{
		THS_IDLE,
		THS_RUNNING,
		THS_PAUSED,
		THS_ERROR,
		THS_STOPED,
	};

}

#endif
