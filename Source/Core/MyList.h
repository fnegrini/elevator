#ifndef MY_LIST_H
#define MY_LIST_H

#include <stdio.h>

template<class TL>
class MyList
{

public:
	template<class TL>
	class MyElement
	{

	public:
		MyElement() {
			pNext = nullptr;
			pPrevious = nullptr;
		}

		~MyElement() {

		}

		TL GetElement() {

			return pElement;
		}

		MyElement<TL>* GetNext() {

			return pNext;
		}
		MyElement<TL>* GetPrevious() {

			return pPrevious;
		}
		void SetElement(TL newVal) {

			pElement = newVal;
		}
		void SetNext(MyElement<TL>* newVal) {

			pNext = newVal;
		}
		void SetPrevious(MyElement<TL>* newVal) {

			pPrevious = newVal;
		}

	private:
		TL pElement;
		MyElement<TL>* pNext;
		MyElement<TL>* pPrevious;
	};


	MyList() {
		pCurrent = nullptr;
		pFirst = nullptr;

	}

	virtual ~MyList() {
		clear();
	}

	MyElement<TL>* GetCurrent() {

		return pCurrent;
	}
	MyElement<TL>* GetFirst() {

		return pFirst;
	}
	int GetLength() {

		return length;
	}
	void SetCurrent(MyElement<TL>* newVal) {

		pCurrent = newVal;
	}
	void SetFirst(MyElement<TL>* newVal) {

		pFirst = newVal;
	}

	void push_back(TL pElement)
	{
		if (NULL != pElement)
		{
			MyElement<TL>* pAux = new MyElement<TL>();
			pAux->SetElement(pElement);

			if (NULL == pFirst)
			{
				pFirst = pAux;
				pCurrent = pFirst;
			}
			else
			{
				pAux->SetPrevious(pCurrent);
				pAux->SetNext(NULL);
				pCurrent->SetNext(pAux);
				pCurrent = pCurrent->GetNext();
			}
			length++;
		}
	}

	void push_front(TL pElement)
	{
		if (NULL != pElement)
		{
			MyElement<TL>* pAux = new MyElement<TL>();
			pAux->SetElement(pElement);

			if (NULL == pFirst)
			{
				pFirst = pAux;
				pCurrent = pFirst;
			}
			else
			{
				pAux->SetPrevious(NULL);
				pAux->SetNext(pFirst);
				pFirst->SetPrevious(pAux);
				pFirst = pFirst->GetPrevious();
			}
			length++;
		}
	}

	void clear()
	{
		MyElement<TL>* pAux = pFirst;
		while (pAux != NULL)
		{
			pAux = pFirst->GetNext();
			delete pFirst;
			pFirst = pAux;
		}
	}

private:
	int length;
	MyElement<TL>* pCurrent;
	MyElement<TL>* pFirst;

};

#endif