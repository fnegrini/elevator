#include "RequestList.h"
#include "Request.h"


Core::RequestList::RequestList()
{
}


bool Core::RequestList::RequestExist(Request * oRequest)
{
	ReqtListInternaliterator it;

	it = std::find(begin(), end(), oRequest);

	return (it != end());

}

bool Core::RequestList::DeleteRequest(Request * oRequest)
{
	ReqtListInternaliterator it;

	it = std::find(begin(), end(), oRequest);

	if (it != end()) {

		erase(it);

		return true;
	}
	else {
		return false;
	}

}

bool Core::RequestList::InsertRequest(Request * oRequest)
{
	if (!RequestExist(oRequest)) {

		push_back(oRequest);

		return true;
	}
	else {
		return false;
	}

}
