#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "Main.h"

namespace Core
{
Main::Main(long lCycleInterval, OutputType eOutput)
{
	MyFactory = FactoryBase::GetFactory(eOutput);

	MyLog = MyFactory->CreateLog();

	MyControler = ControlerBase::FactoryMethod(MyFactory->GetControlerTypeInteractive(), "Main Controler", MyLog, MyFactory);

	MyInput = MyFactory->CreateInputPerson("Input process", MyLog, MyControler);

	CycleInterval = lCycleInterval;

	ComponentBase::SetCycleInterval(CycleInterval);
}

Main::~Main()
{

	delete MyInput;

	delete MyControler;

	delete MyLog;

	delete MyFactory;

}

ControlerBase * Main::GetControler()
{
	return MyControler;
}

InputPersonBase * Main::GetInputPerson()
{
	return MyInput;
}

LogBase * Main::GetLog()
{
	return MyLog;
}

void Main::Setup()
{
	if ((MyControler == nullptr) || (MyInput == nullptr)) return;

	MyFactory->SetupEnvironmentInteractive(MyControler);

	MyInput->CreateRequestsInteractive();

}

void Main::Execute()
{

	if ((MyControler == nullptr) || (MyInput == nullptr)) return;

	MyControler->StartComponent();

	MyInput->StartComponent();

}


void Main::Stop()
{

	if ((MyControler == nullptr) || (MyInput == nullptr)) return;

	MyInput->StopComponent();

	MyControler->StopComponent();

}

void Main::LogStatistics()
{
	if ((MyControler == nullptr) || (MyInput == nullptr)) return;

	MyInput->LogStatistics();

	MyControler->LogStatistics();

}

}  // namespace Core
