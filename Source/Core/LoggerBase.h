#ifndef CORE_LOGGER_BASE_H
#define CORE_LOGGER_BASE_H

#include "MessageType.h"
#include "LogBase.h"

namespace Core
{

class LoggerBase
{
private:
	
	std::string MyName;

	LogBase *MyLogBase;

protected:
	virtual void LogMessageInternal(MessageType Type, const char *Message);

	virtual LogBase *GetLogBase();

public:
	LoggerBase(const char *sName = "", LogBase *oLogBase = nullptr);

	void SetName(const char *sName);

	const char *GetName();

	~LoggerBase();
};

}
#endif
