#include "FactoryBase.h"
#include "../Console/FactoryStream.h"
#include "../Windows/FactoryWindows.h"
#include "LogBase.h"
#include "InputPersonBase.h"

namespace Core {

FactoryBase::FactoryBase()
{
}

FactoryBase * FactoryBase::GetFactory(OutputType eOutput)
{
	switch (eOutput)
	{
	case OT_STREAM:
		return new Console::FactoryStream();
		break;
	case OT_WINDOW:
		return new Windows::FactoryWindows();
	default:
		return nullptr;
		break;
	}
	return nullptr;
}





}