#ifndef CORE_COMPONENTBASECYCLESTATE_H
#define CORE_COMPONENTBASECYCLESTATE_H

#include <list>

namespace Core
{
	enum ComponentCycleState
	{
		CCS_IDLE,
		CCS_RUNNING,
	};

}

#endif

