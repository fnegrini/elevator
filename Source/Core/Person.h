#ifndef CORE_PERSON_H
#define CORE_PERSON_H

#include <list>
#include <fstream>

#include "ComponentBase.h"
#include "Request.h"
#include "PersonState.h"
#include "PersonAbstractInterface.h"
#include "FactoryBase.h"

namespace Core
{


class ControlerBase;

class Person : public ComponentBase
{

private:
	
	RequestList MyRequestList;

	ControlerBase *MyControler;

	PersonState MyState;

	FactoryBase *MyFactory;

	PersonAbstractInterface *MyInterface;

protected:

	virtual void Run();

	virtual void RunRequest(Request *oRequest);

	virtual void WaitUpToFinishRequest(Request *oRequest);

	string StateToString();

	virtual void SetState(PersonState eState);

public:

	Person(const char *sName = "", LogBase *oLogBase = nullptr, ControlerBase *oControler = nullptr, FactoryBase *oFactory = nullptr);

	~Person();

	Request *CreateRequest(int iFloorFrom = 0, int iFloorTo = 0, int iCyclesWait = 0);

	virtual void StopComponent();

	PersonState GetState();

	void LogStatistics(std::ofstream *File);


};


}  // namespace Core

#endif
