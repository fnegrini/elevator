#ifndef CORE_INPUT_PERSON_BASE_H
#define CORE_INPUT_PERSON_BASE_H

#include "PersonList.h"
#include "ControlerBase.h"
#include "ComponentBase.h"
#include "InputRequest.h"

namespace Core
{

class FactoryBase;

class InputPersonBase: public ComponentBase
{
private:
	FactoryBase *MyFactory;

	PersonList MyPersonList;
	
	ControlerBase *MyControler;

protected:

	virtual void Run();

	PersonList *GetPersonList();

public:

	InputPersonBase(const char *sName = "", LogBase *oLogBase = nullptr, ControlerBase *oControlerBase = nullptr, FactoryBase *oFactory = nullptr);

	virtual Person *CreatePerson(const char *PersonName, InputRequestList *Requests);

	virtual void LogStatistics();

	void SetControler(ControlerBase *oControler);

	virtual void CreateRequestsInteractive() abstract;


};

}  // namespace Core
#endif
