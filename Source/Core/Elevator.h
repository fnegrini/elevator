#ifndef CORE_ELEVATOR_H
#define CORE_ELEVATOR_H

#include "ComponentBaseCycleThread.h"
#include "ElevatorState.h"
#include "FactoryBase.h"
#include "ElevatorAbstractInterface.h"
#include "FactoryBase.h"

namespace Core
{


class Person;

class PersonList;

class Elevator : public ComponentBaseCycleThread
{
private:
	int Floor;

	int TotalCyclesUp;

	int TotalCyclesDown;

	ElevatorState MyState;

	int CyclesPerFloor;

	int CyclesPerDoor;

	int MinFloor;
	
	int MaxFloor;

	unsigned int Capacity;

	PersonList *MyPersonList;

	FactoryBase *MyFactory;

	ElevatorAbstractInterface *MyInterface;

protected:
	virtual std::string StateToString();

	virtual void DoAfterCycleCountEnds(); 

	virtual void DoCycleStepMovingUp();

	virtual void DoCycleStepMovingDown();

	virtual void DoCycleStepClosingDoor();
		
	virtual void DoCycleStepOpeningDor();

	virtual void SetState(ElevatorState iState);

	virtual void IncFloor();

	virtual void DecFloor();

	virtual void LogFloor();

public:
	Elevator(const char *sName = "", LogBase *oLogBase = nullptr, int iMinFloor=0, int iMaxFloor=0, int iCyclesPerFloor=5, int iCyclesPerDoor=2, unsigned int Capacity = 15, FactoryBase *oFactory = nullptr);

	~Elevator();

	int GetTotalCyclesUp();

	int GetTotalCyclesDown();

	void ResetCyclesCount();

	int GetFloor();

	ElevatorState GetState();

	void SetMinFloor(int iMinFloor);

	int GetMinFloor();

	void SetMaxFloor(int iMaxFloor);

	int GetMaxFloor();

	void SetCyclesPerFloor(int iCyclesPerFloor);

	int GetCyclesPerFloor();

	void SetCyclesPerDoor(int iCyclesPerDoor);

	int GetCyclesPerDoor();

	void SetCapacity(unsigned int iCapacity);

	unsigned int GetCapacity();

	bool MoveUp();

	bool MoveDown();

	bool OpenDoor();

	bool CloseDoor();

	bool AddPerson(Person *oPerson);

	bool ReleasePerson(Person *oPerson);

};

}  // namespace Core
#endif
