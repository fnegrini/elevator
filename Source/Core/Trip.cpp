#include "Trip.h"
#include "Request.h"

namespace Core
{

bool CompareFloorAscending(int first, int second) {

	return first < second;
}

bool CompareFloorDescending(int first, int second) {

	return first >= second;
}


void Trip::CommitFloorRequests(int iFloor)
{
	ReqtListInternaliterator it;
	Request *oRequestAux;

	for (it = MyRequests.begin(); it != MyRequests.end(); it++) {
		oRequestAux = *it;
		
		if (oRequestAux->GetFloorFrom() == iFloor) {
			oRequestAux->GoToTrip(this, iFloor);
		}

		if (oRequestAux->GetFloorTo() == iFloor) {
			oRequestAux->ReleaseTrip(iFloor);
		}
	}
}

Trip::Trip(TripDirection eDirection, ElevatorControler *oControler)
{

	SetDirection(eDirection);

	SetState(TS_BLOCKED);

	MyControler = oControler;
}

bool Trip::FloorInTrip(int Floor)
{
	return (std::find(MyFloorList.begin(), MyFloorList.end(), Floor) != MyFloorList.end());
}

void Trip::AddFloorToEnd(int Floor)
{
	MyFloorList.push_back(Floor);
}


bool Trip::RemoveFloor(int Floor)
{
	return (std::find(MyFloorList.begin(), MyFloorList.end(), Floor) != MyFloorList.end());
}

bool Trip::RemoveStage(unsigned int Index)
{
	FloorListIterator it;


	if (MyFloorList.size() > Index)
	{
		it = std::next(MyFloorList.begin(), Index);

		if (it != MyFloorList.end()) {

			MyFloorList.erase(it);
		}

		return true;

	}
	else {

		return false;
	}

}

RequestList * Trip::GetRequestList()
{
	return &MyRequests;
}

bool Trip::InsertRequest(Request * oRequest)
{
	bool Result;

	
	Result = MyRequests.InsertRequest(oRequest);

	if(Result){

		AddFloorToEnd(oRequest->GetFloorFrom());

		AddFloorToEnd(oRequest->GetFloorTo());

	}

	return Result;


}

ElevatorControler * Trip::GetControler()
{
	return MyControler;
}

bool Trip::TripFinished()
{
	return (MyFloorList.size() == 0);
}

int Trip::GetNextFloorToGo()
{
	int NextFloor;

	if (!TripFinished()) {

		NextFloor = MyFloorList.front();
		MyFloorList.pop_front();
	}
	else
	{
		NextFloor = -9999;
	}

	return NextFloor;
}

void Trip::SetState(TripState eTrip)
{

	MyState = eTrip;
}

TripState Trip::GetState()
{

	return MyState;
}

bool Trip::ReleaseTrip()
{
	if (MyState != TS_BLOCKED) {

		return false;
	}
	else {

		SetState(TS_RELEASED);
		return true;
	}
}

bool Trip::PrepareTrip()
{
	if (MyState != TS_RELEASED) {

		return false;
	}
	else {

		SetState(TS_PREPARING);

		return true;
	}
}



bool Trip::StartTrip()
{
	if (MyState != TS_RELEASED) {
		return false;
	} else {
		SetState(TS_RUNNING);
		return true;
	}
}

bool Trip::EndTrip()
{
	if (MyState != TS_RUNNING) {
		return false;
	} else {
		SetState(TS_FINISHED);
		SetDirection(TD_UNASSIGNED);
		return true;
	}

}

void Trip::SetDirection(TripDirection eDirection)
{
	MyDirection = eDirection;
}

TripDirection Trip::GetDirection()
{
	return MyDirection;
}

void Trip::SortFloorsByDirection()
{
	
	if (MyDirection == TD_TRIP_UP)
		MyFloorList.sort(CompareFloorAscending);
	else if (MyDirection == TD_TRIP_DOWN)
		MyFloorList.sort(CompareFloorDescending);

}

}  // namespace Core

