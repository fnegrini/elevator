#ifndef CORE_MESSAGETYPE_H
#define CORE_MESSAGETYPE_H

#include <list>

namespace Core
{
	enum MessageType
	{
		MSG_UNDEFINED,
		MSG_NOTIFICATION,
		MSG_ERROR,
		MSG_WARNING,
		MSG_DEBUG
	};

	typedef std::list<MessageType> MessageTypeList;
}


#endif