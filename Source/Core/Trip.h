#ifndef CORE_TRIP_H
#define CORE_TRIP_H

#include <list>
#include <string>

#include "RequestList.h"
#include "TripDirection.h"
#include "TripState.h"

namespace Core
{

typedef std::list<int> FloorList;
typedef std::list<int>::iterator FloorListIterator;


class ElevatorControler;

class Trip
{
private:

	FloorList MyFloorList;

	RequestList MyRequests;

	TripDirection MyDirection;

	TripState MyState;

	ElevatorControler *MyControler;

protected:

	void SetState(TripState eTrip);

	void SetDirection(TripDirection eDirection);


public:

	Trip(TripDirection eDirection= TD_UNASSIGNED, ElevatorControler *oControler =nullptr);

	bool FloorInTrip(int Floor);

	void AddFloorToEnd(int Floor);

	bool RemoveFloor(int Floor);

	bool RemoveStage(unsigned int Index);

	RequestList *GetRequestList();

	bool InsertRequest(Request *oRequest);

	ElevatorControler *GetControler();

	bool TripFinished();

	int GetNextFloorToGo();

	virtual void CommitFloorRequests(int iFloor);

	TripState GetState();

	bool ReleaseTrip();

	bool PrepareTrip();

	bool StartTrip();

	bool EndTrip();

	TripDirection GetDirection();

	void SortFloorsByDirection();

};

typedef std::list<Trip*> TripList;

typedef std::list<Trip*>::iterator TripListIterator;

}  // namespace Core
#endif
