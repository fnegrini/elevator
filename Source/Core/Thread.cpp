#include "Thread.h"


namespace Core {

// refer�ncia para a vari�vel est�tica;
pthread_mutex_t Thread::_mutex = nullptr;

Thread::Thread(){
	state = THS_IDLE;
}

Thread::~Thread(){

	StopThread();

	if (nullptr != Thread::_mutex)
		pthread_mutex_destroy(&Thread::_mutex);
}

void Thread::JoinThread(){
	int status = pthread_join(_threadID, nullptr);
	if (status != 0)
		PrintError("comando join falhou.");
}


void Thread::LockThread(){
	if (nullptr == Thread::_mutex)
		pthread_mutex_init(&Thread::_mutex, nullptr);
	pthread_mutex_lock(&Thread::_mutex);
}


void Thread::PrintError(const std::string msg){
	LockThread();     // p�ra para a mensagem;
	PrintErrorInternal(msg);
	UnlockThread();
}


void Thread::Run(){

}

void Thread::RunInternal()
{
	while (state!= THS_STOPED)
	{
		if (state == THS_RUNNING) {
			Run();
		}

		YieldThread(); // Baixa prioridade apos execucao
	}
}


void* Thread::RunThread(void* pThread){
	Thread* sThread = static_cast<Thread*>(pThread);
	if (nullptr == sThread){
		cout << "thread falhou." << endl;
	}
	else{
		sThread->RunInternal();/* executa a thread; */
	}
	return nullptr;
}

void Thread::PrintErrorInternal(const string msg)
{
	cout << "Error: " << msg << endl;
}


void Thread::StartThread(){

	if (state != THS_IDLE) return;

	// inicia o atributo;
	int status = pthread_attr_init(&_tAttribute);

	status = pthread_attr_setscope(&_tAttribute, PTHREAD_SCOPE_SYSTEM);

	if (status != 0) {
		PrintError("falha ao iniciar atributo da thread.");
		SetThreadState(THS_ERROR);
	}

	// cria uma thread;
	SetThreadState(THS_RUNNING);

	status = pthread_create(&_threadID, &_tAttribute, Thread::RunThread, (void*)this);
	if (status != 0) {
		PrintError("falha ao iniciar a thread.");
		SetThreadState(THS_ERROR);
	}

	// destr�i o atributo;
	status = pthread_attr_destroy(&_tAttribute);
	if (status != 0) {
		PrintError("falha ao destruir atributo da thread.");
		SetThreadState(THS_ERROR);
	}

}

void Thread::UnlockThread(){
	if (nullptr != Thread::_mutex)
		pthread_mutex_unlock(&Thread::_mutex);
}


void Thread::YieldThread(){
	sched_yield();
}

void Thread::PauseThread()
{
	if (state == THS_RUNNING) {
		SetThreadState(THS_PAUSED);
	}
}

void Thread::ResumeThread()
{
	if (state == THS_PAUSED) {
		SetThreadState(THS_RUNNING);
	}
}

void Thread::StopThread()
{
	if (state == THS_RUNNING) {
		
		SetThreadState(THS_STOPED);

		JoinThread();
	}

}

void Thread::SetThreadState(Threadstate eState)
{
	LockThread();
	state = eState;
	UnlockThread();
}

Threadstate Thread::GetThreadState()
{
	return state;
}

}