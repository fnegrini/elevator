#ifndef CORE_MAIN_H
#define CORE_MAIN_H

#include "ControlerBase.h"
#include "InputPersonBase.h"
#include "FactoryBase.h"
#include "ControlerType.h"

namespace Core
{
class Main
{
private:

	FactoryBase *MyFactory;

	LogBase *MyLog;

	ControlerBase *MyControler;

	InputPersonBase *MyInput;

	long CycleInterval;
public:
	Main(long lCycleInterval = 1000, OutputType eOutput = OT_STREAM);

	~Main();

	ControlerBase *GetControler();

	InputPersonBase *GetInputPerson();

	LogBase *GetLog();

	void Setup();

	void Execute();

	void Stop();

	void LogStatistics();

};

}  // namespace Core
#endif
