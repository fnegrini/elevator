#ifndef CORE_TRIPDIRECTION_H
#define CORE_TRIPDIRECTION_H


namespace Core
{
	enum TripDirection
	{
		TD_UNASSIGNED,
		TD_TRIP_UP,
		TD_TRIP_DOWN,
	};

}
#endif
