#ifndef CORE_PERSON_ABS_INTERFACE_H
#define CORE_PERSON_ABS_INTERFACE_H

namespace Core {

class Person;

class Request;

class Elevator;

class PersonAbstractInterface
{
private:
	Person *MyPerson;

protected:

	Person *GetPerson();

public:

	PersonAbstractInterface(Person *oPerson = nullptr);

	~PersonAbstractInterface();

	virtual void SimulatingOperation(Request *oRequest) abstract;

	virtual void RequestStarted(Request *oRequest) abstract;

	virtual void IntoElevator(Request *oRequest, Elevator *oElevator) abstract;

	virtual void LeaveElevator(Request *oRequest) abstract;

	virtual void RequestFinished(Request *oRequest) abstract;
};

}

#endif
