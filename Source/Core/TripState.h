#ifndef CORE_TRIPSTATE_H
#define CORE_TRIPSTATE_H


namespace Core
{
	enum TripState
	{
		TS_BLOCKED,
		TS_RELEASED,
		TS_PREPARING,
		TS_RUNNING,
		TS_FINISHED,
	};

}
#endif
