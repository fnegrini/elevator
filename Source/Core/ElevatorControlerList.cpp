#include "ElevatorControlerList.h"


namespace Core {


ElevatorControlerList::ElevatorControlerList()
{
}


ElevatorControlerList::~ElevatorControlerList()
{
}

ElevatorControlerIterator * Core::ElevatorControlerList::GetCurrent()
{
	return MyList.GetCurrent();
}

ElevatorControlerIterator * ElevatorControlerList::GetFirst()
{
	return MyList.GetFirst();
}

int ElevatorControlerList::GetLength()
{
	return MyList.GetLength();
}

void ElevatorControlerList::push_back(ElevatorControler * pElement)
{
	MyList.push_back(pElement);
}

void ElevatorControlerList::push_front(ElevatorControler* pElement)
{
	MyList.push_front(pElement);
}

void ElevatorControlerList::clear()
{
	MyList.clear();
}

}