#ifndef CORE_CONTROLER_BASE_H
#define CORE_CONTROLER_BASE_H

#include "Trip.h"
#include "ElevatorControlerList.h"
#include "InputPersonBase.h"
#include "Person.h"
#include "LoggerBase.h"
#include "ControlerType.h"

namespace Core
{

class FactoryBase;

class ControlerBase : public LoggerBase
{
private:

	FactoryBase *MyFactory;

	ElevatorControlerList MyControlerList;

	int BalanceControl;

protected:
	
	virtual Trip *GetIdleTrip(Request *oRequest);
	virtual Trip *GetIdleElevator(Request *oRequest);
	virtual Trip *GetOccupedElevatorBalanced(Request *oRequest);

	virtual Trip *DetermineControlerToRequest(Request *oRequest);

public:
	static ControlerBase *FactoryMethod(ControlerType oType = CT_STANDARD, const char *sName = "", LogBase *oLogBase = nullptr, FactoryBase *oFactory = nullptr);

	ControlerBase(const char *sName = "", LogBase *oLogBase = nullptr, FactoryBase *oFactory = nullptr);

	~ControlerBase();

	virtual ElevatorControler *CreateControler(const char *sName = "", const char *sElevatorName = "", int iMinFloor = 0, int iMaxFloor = 0, int iCyclesPerFloor = 5, int iCyclesPerDoor = 2, unsigned int iCapacity = 15);

	virtual ElevatorControler *InsertRequest(Request *oRequest);

	virtual void StartComponent();

	virtual void StopComponent();

	virtual void LogStatistics();


};

}  // namespace Core
#endif
