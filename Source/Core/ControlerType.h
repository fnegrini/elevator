#ifndef CORE_CONTROLERTYPE_H
#define CORE_CONTROLERTYPE_H


namespace Core
{
	enum ControlerType
	{
		CT_STANDARD,
		CT_CUP_CDOWN,
		CT_CUP_SDOWN,
		CT_SUP_CDOWN,
		CT_SUP_SDOWN,

	};

}

#endif
