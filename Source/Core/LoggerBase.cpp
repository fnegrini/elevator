#include "LoggerBase.h"

namespace Core
{

void LoggerBase::LogMessageInternal(MessageType Type, const char * Message)
{
	if (MyLogBase != nullptr) {
		MyLogBase->LogMessage(this, Type, Message);
	}
}

LogBase * LoggerBase::GetLogBase()
{
	return MyLogBase;
}
LoggerBase::LoggerBase(const char *sName, LogBase *oLogBase)
{
	MyLogBase = oLogBase;
	SetName(sName);
}

void LoggerBase::SetName(const char *sName)
{
	if (sName != nullptr) {
		MyName = sName;
	}
	else {
		MyName = "";
	}
}

const char * LoggerBase::GetName()
{
	return MyName.c_str();
}


LoggerBase::~LoggerBase()
{
}

}