#ifndef CORE_PERSON_LIST_H
#define CORE_PERSON_LIST_H

#include <list>
#include "Person.h"

namespace Core {

typedef std::list<Person*> PersonListInternal;

typedef PersonListInternal::iterator PersonListIterator;

class PersonList : public PersonListInternal
{
private:
	PersonListInternal MyList;

public:
	PersonList();

	~PersonList();


};



}
#endif