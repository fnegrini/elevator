#ifndef CORE_REQUESTLIST_H
#define CORE_REQUESTLIST_H

#include <list>

namespace Core
{

class Request;

typedef std::list<Request*> ReqtListInternal;
typedef std::list<Request*>::iterator ReqtListInternaliterator;


class RequestList: public ReqtListInternal
{

private:

protected:

public:

	RequestList();

	bool RequestExist(Request *oRequest);

	bool DeleteRequest(Request *oRequest);

	bool InsertRequest(Request *oRequest);


};
}

#endif