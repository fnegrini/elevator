#include "InputRequest.h"

namespace Core
{

InputRequest::InputRequest(int iFloorFrom, int iFloorTo, int iCyclesBeforeStart)
{
	FloorFrom = iFloorFrom;
	FloorTo = iFloorTo;
	CyclesBeforeStart = iCyclesBeforeStart;
}


InputRequest::~InputRequest()
{
}

int InputRequest::GetFloorFrom()
{
	return FloorFrom;
}

int InputRequest::GetFloorTo()
{
	return FloorTo;
}

int InputRequest::GetCyclesBeforeStart()
{
	return CyclesBeforeStart;
}

void InputRequest::SetFloorFrom(int iFloorFrom)
{
	FloorFrom = iFloorFrom;
}

void InputRequest::SetFloorTo(int iFloorTo)
{
	FloorTo = iFloorTo;
}

void InputRequest::SetCyclesBeforeStart(int iCyclesBeforeStart)
{
	CyclesBeforeStart = iCyclesBeforeStart;
}

}