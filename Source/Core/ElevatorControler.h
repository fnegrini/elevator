#ifndef CORE_ELEVATOR_CONTROLER_H
#define CORE_ELEVATOR_CONTROLER_H

#include <vector>


#include "ComponentBase.h"
#include "Elevator.h"
#include "ElevatorControlerAction.h"
#include "Trip.h"
#include "ElevatorControlerAbstractInterface.h"
#include "MyList.h"

namespace Core
{

class FactoryBase;

class ElevatorControler : public ComponentBase
{
private:
	FactoryBase *MyFactory;

	Elevator *MyElevator;

	ElevatorControlerAction MyAction;

	TripList MyTripList;

	ElevatorControlerAbstractInterface *MyInterface;

protected:

	virtual std::string ActionToString();

	virtual void SetAction(ElevatorControlerAction eAction);

	virtual bool CheckIdle();

	virtual void Run();

	void MoveElevatorToFloor(Trip *oTrip, int Floor);

	void ElevatorOpenDoor();

	void ElevatorCloseDoor();

	virtual bool GotoFloor(Trip *oTrip, int Floor);
	
	virtual bool OpenDoor();

	virtual bool CloseDoor();

	virtual void WaitReleaseElevator();

	virtual void CommitFloorRequests(int Floor, Trip *oTrip);

	Trip *GetNextTrip();

	void ExecuteTrip(Trip *oTrip);

	virtual bool AddTrip(Trip *oTrip);

	virtual void CheckTripSuccess(Trip *oTrip);

public:
	ElevatorControler(const char *sName = "", const char *sElevatorName = "", LogBase *oLogBase = nullptr, int iMinFloor = 0, int iMaxFloor = 0, int iCyclesPerFloor = 5, int iCyclesPerDoor = 2, unsigned int iCapacity = 15, FactoryBase *oFactory = nullptr);
	
	~ElevatorControler();

	virtual void StartComponent();

	virtual void StopComponent();

	ElevatorControlerAction GetAction();

	Elevator *GetElevator();

	Trip *CreateTrip(TripDirection eDirection);

	Trip *GetIdleTrip(TripDirection eDirection);


};

//typedef MyList<ElevatorControler*> ElevatorControlerList;

//typedef MyList<ElevatorControler*>::MyElement<ElevatorControler*> ElevatorControlerIterator;

}  // namespace Core

#endif
