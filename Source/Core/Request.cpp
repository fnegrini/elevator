#include "Request.h"
#include "RequestState.h"
#include "ControlerBase.h"
#include "Trip.h"
#include "Person.h"

#include <iostream> 
	using namespace std;

namespace Core
{


void Request::StartRequest()
{
	SetState(RQS_WAITING);
}

void Request::SetState(RequestState eState)
{

	string OldStateStr;
	string msg;

	OldStateStr = StateToString();

	if(MyPerson!=nullptr) MyPerson->LockThread();
	state = eState;
	if (MyPerson != nullptr) MyPerson->UnlockThread();

	msg = string("State changed from ") + OldStateStr + string(" to ") + StateToString();

}

string Request::StateToString()
{
	string StateStr;

	switch (state)
	{
	case Core::RQS_IDLE:
		StateStr = "Idle";
		break;

	case Core::RQS_WAITING:
		StateStr = "Waiting";
		break;

	case Core::RQS_ON_TRIP:
		StateStr = "On Trip";
		break;

	case Core::RQS_DONE:
		StateStr = "Done";
		break;

	default:
		StateStr = "Undefined";
		break;
	}
	return StateStr;
}

Request::Request(int iFloorFrom, int iFloorTo, int iCyclesBeforeStart, Person *oPerson)
{

	FloorFrom = iFloorFrom;
	FloorTo = iFloorTo;
	CyclesBeforeStart = iCyclesBeforeStart;

	CyclesWaitingOut = 0;
	CyclesWaitingIn = 0;

	CyclesCount = 0;

	MyTrip = nullptr;
	MyPerson = oPerson;

	SetState(RQS_IDLE);

}

int Request::GetCyclesWaitingOut()
{
	return CyclesWaitingOut;
}

int Request::GetCyclesWaitingIn()
{
	return CyclesWaitingIn;
}

bool Request::GoToTrip(Trip * oTrip, int Floor)
{
	Elevator *ElevatorAux;
	string msg;
	
	if (state != RQS_WAITING) {
		if (MyPerson != nullptr) MyPerson->LogMessageInThread(MSG_ERROR, "Request not waiting for trip yet");
		return false;
	}

	if (MyTrip != nullptr) {

		if (MyPerson != nullptr) MyPerson->LogMessageInThread(MSG_ERROR, "Request already in a trip");
		return false;

	}

	if (Floor != FloorFrom) {

		msg = string("Elevator in Floor ") + string(to_string(Floor)) + string(" but person waits on ") +
			string(to_string(FloorFrom)) + string(" floor.");

		if (MyPerson != nullptr) MyPerson->LogMessageInThread(MSG_ERROR, msg.c_str());

		return false;
	}
	
	MyTrip = oTrip;

	ElevatorAux = oTrip->GetControler()->GetElevator();

	if (MyPerson != nullptr) {

		if (ElevatorAux->AddPerson(MyPerson)) {

			SetState(RQS_ON_TRIP);

			msg = string("Person get into the elevator ") + string(ElevatorAux->GetName());

			MyPerson->LogMessageInThread(MSG_NOTIFICATION, msg.c_str());

			return true;
		} else {

			return false;

		}

	}

	return false;

}

bool Request::ReleaseTrip(int Floor)
{
	Elevator *ElevatorAux;
	string msg;

	if (state != RQS_ON_TRIP) {
		if (MyPerson != nullptr) MyPerson->LogMessageInThread(MSG_ERROR, "Person not in a trip");
		return false;
	}

	if (MyTrip == nullptr) {
		if (MyPerson != nullptr) MyPerson->LogMessageInThread(MSG_ERROR, "Person noa assigned to a trip.");
		return false;
	}

	if (FloorTo != Floor) {

		msg = string("Elevator in Floor ") + string(to_string(Floor)) + string(" but person wants to go to ") +
			string(to_string(FloorTo)) + string(" floor.");

		if (MyPerson != nullptr) MyPerson->LogMessageInThread(MSG_ERROR, msg.c_str());

		return false;
	}
	
	ElevatorAux = MyTrip->GetControler()->GetElevator();

	if (MyPerson != nullptr) {

		if (ElevatorAux->ReleasePerson(MyPerson)) {

			MyTrip = nullptr;

			SetState(RQS_DONE);

			msg = string("Person release elevator ") + string(ElevatorAux->GetName());

			MyPerson->LogMessageInThread(MSG_NOTIFICATION, msg.c_str());

			return true;

		}
		else {

			return false;

		}

	}

	return false;

}


TripDirection Request::GetDirection()
{
	if (FloorFrom > FloorTo)
		return TD_TRIP_DOWN;
	else
		return TD_TRIP_UP;

}

int Request::GetFloorFrom()
{
	return FloorFrom;
}

int Request::GetFloorTo()
{
	return FloorTo;
}

int Request::GetCyclesBeforeStart()
{
	return CyclesBeforeStart;
}

RequestState Request::GetState()
{
	return state;
}

void Request::SimulateJob()
{
	int CyclesCount = CyclesBeforeStart;

	SetState(RQS_SIMULATING_JOB);

	while (CyclesCount > 0) {

		WaitUpToOneCycle();

		CyclesCount--;
	}
}

void Request::IncCyclesWaitingIn()
{
	CyclesWaitingIn++;
}

void Request::IncCyclesWaitingOut()
{
	CyclesWaitingOut++;
}

const char * Request::GetPersonName()
{
	if (MyPerson != nullptr)
		return MyPerson->GetName();
	else
		return "";
}

void Request::WaitUpToOneCycle()
{
	if(MyPerson!=nullptr) MyPerson->WaitUpToOneCycle();
}

}