#ifndef CORE_REQUESTSTATE_H
#define CORE_REQUESTSTATE_H

#include <list>

namespace Core
{
	enum RequestState
	{
		RQS_IDLE,
		RQS_SIMULATING_JOB,
		RQS_WAITING,
		RQS_ON_TRIP,
		RQS_DONE,
	};

}

#endif
