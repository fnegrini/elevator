#include "Elevator.h"
#include "PersonList.h"

namespace Core
{

Elevator::Elevator(const char * sName, LogBase * oLogBase, int iMinFloor, int iMaxFloor, int iCyclesPerFloor, int iCyclesPerDoor, unsigned int iCapacity, FactoryBase *oFactory)
:ComponentBaseCycleThread(sName, oLogBase)
{
	Floor = 0;
	TotalCyclesUp = 0;
	TotalCyclesDown = 0;
	MyState = ES_STOPED_DOOR_CLOSED;
	MyInterface = nullptr;

	MyFactory = oFactory;

	SetMinFloor(iMinFloor);
	SetMaxFloor(iMaxFloor);
	SetCyclesPerFloor(iCyclesPerFloor);
	SetCyclesPerDoor(iCyclesPerDoor);
	SetCapacity(iCapacity);

	ResetCyclesCount();

	if (MyFactory != nullptr) {

		MyInterface = MyFactory->CreateElevatorAbstractInterface(this);
	}

	MyPersonList = new PersonList();
}

Elevator::~Elevator()
{
	if(MyInterface!=nullptr)
		delete MyInterface;

	delete MyPersonList;
}

string Elevator::StateToString()
{
	string Result;

	switch (MyState)
	{
	case Core::ES_STOPED_DOOR_CLOSED:
		Result = "Door Closed";
		break;
	case Core::ES_STOPED_DOOR_OPENED:
		Result = "Door Opened";
		break;
	case Core::ES_MOVING_UP:
		Result = "Moving Up";
		break;
	case Core::ES_MOVING_DOWN:
		Result = "Moving Down";
		break;
	case Core::ES_CLOSING_DOOR:
		Result = "Closing Door";
		break;
	case Core::ES_OPENING_DOOR:
		Result = "Opening Door";
		break;
	default:
		Result = "Undefined";
		break;
	}
	return Result;

}
void Elevator::DoAfterCycleCountEnds()
{

	switch (MyState)
	{
	case Core::ES_STOPED_DOOR_CLOSED:
		break;
	case Core::ES_STOPED_DOOR_OPENED:
		break;
	case Core::ES_MOVING_UP:
		DoCycleStepMovingUp();
		break;
	case Core::ES_MOVING_DOWN:
		DoCycleStepMovingDown();
		break;
	case Core::ES_CLOSING_DOOR:
		DoCycleStepClosingDoor();
		break;
	case Core::ES_OPENING_DOOR:
		DoCycleStepOpeningDor();
		break;
	default:
		break;
	}
}

void Elevator::DoCycleStepMovingUp()
{
	SetState(ES_STOPED_DOOR_CLOSED);
	IncFloor();
	TotalCyclesUp += CyclesPerFloor;
}

void Elevator::DoCycleStepMovingDown()
{
	SetState(ES_STOPED_DOOR_CLOSED);
	DecFloor();
	TotalCyclesDown += CyclesPerFloor;

}

void Elevator::DoCycleStepClosingDoor()
{
	SetState(ES_STOPED_DOOR_CLOSED);

	LogMessageInThread(MSG_NOTIFICATION, "Door is closed");

}

void Elevator::DoCycleStepOpeningDor()
{
	SetState(ES_STOPED_DOOR_OPENED);

	LogMessageInThread(MSG_NOTIFICATION, "Door is opened.");
}

void Elevator::SetState(ElevatorState iState)
{
	string Message;
	string OldStateStr;

	OldStateStr = StateToString();
	MyState = iState;

	Message = string("State chanded from ") + OldStateStr + string(" to ") + StateToString();

	LogMessageInThread(MSG_DEBUG, Message.c_str());


}

void Elevator::IncFloor()
{
	Floor++;
	LogFloor();

}

void Elevator::DecFloor()
{
	Floor--;
	LogFloor();
}

void Elevator::LogFloor()
{
	string Message = string("Elevator arrived to floor ") + string(to_string(Floor));

	if (MyInterface != nullptr)
		MyInterface->ArrivedToFloor(Floor);

	LogMessageInThread(MSG_NOTIFICATION, Message.c_str());
}



int Elevator::GetTotalCyclesUp()
{
	return TotalCyclesUp;
}

int Elevator::GetTotalCyclesDown()
{
	return TotalCyclesDown;
}

void Elevator::ResetCyclesCount()
{
	TotalCyclesUp = 0;
	TotalCyclesDown = 0;
}

int Elevator::GetFloor()
{
	return Floor;
}

ElevatorState Elevator::GetState()
{
	return MyState;
}

void Elevator::SetMinFloor(int iMinFloor)
{
	MinFloor = iMinFloor;
}

int Elevator::GetMinFloor()
{
	return MinFloor;
}

void Elevator::SetMaxFloor(int iMaxFloor)
{
	MaxFloor = iMaxFloor;
}

int Elevator::GetMaxFloor()
{
	return MaxFloor;
}

void Elevator::SetCyclesPerFloor(int iCyclesPerFloor)
{
	CyclesPerFloor = iCyclesPerFloor;
}

int Elevator::GetCyclesPerFloor()
{
	return CyclesPerFloor;
}

void Elevator::SetCyclesPerDoor(int iCyclesPerDoor)
{
	CyclesPerDoor = iCyclesPerDoor;
}

int Elevator::GetCyclesPerDoor()
{
	return CyclesPerDoor;
}

void Elevator::SetCapacity(unsigned int iCapacity)
{
	Capacity = iCapacity;
}

unsigned int Elevator::GetCapacity()
{
	return Capacity;
}

bool Elevator::MoveUp()
{
	switch (MyState)
	{
	case Core::ES_STOPED_DOOR_CLOSED:
		if (Floor >= MaxFloor) {
			LogMessageInThread(MSG_ERROR, "Already on last floor");
			return false;
		}
		else {

			SetState(ES_MOVING_UP);
			WaitCyclesAssyncron(CyclesPerFloor);
			if (MyInterface != nullptr)
				MyInterface->MovingUp();

			return true;
		}
		break;

	case Core::ES_STOPED_DOOR_OPENED:
		LogMessageInThread(MSG_ERROR, "Can not move up. Dor is opened.");
		return false;
		break;

	case Core::ES_MOVING_UP:
		LogMessageInThread(MSG_WARNING, "Already moving up.");
		return true;
		break;

	case Core::ES_MOVING_DOWN:
		LogMessageInThread(MSG_ERROR, "Can not move up. Elevator is moving Down.");
		return false;

	case Core::ES_CLOSING_DOOR:
		LogMessageInThread(MSG_ERROR, "Can not move up. Dor is closing");
		return false;

	case Core::ES_OPENING_DOOR:
		LogMessageInThread(MSG_ERROR, "Can not move up. Dor is opening");
		return false;

	default:
		return false;
		break;
	}
	return false;
}

bool Elevator::MoveDown()
{
	switch (MyState)
	{
	case Core::ES_STOPED_DOOR_CLOSED:
		if (Floor <= MinFloor) {
			LogMessageInThread(MSG_ERROR, "Already on first floor");
			return false;
		}
		else {

			SetState(ES_MOVING_DOWN);
			WaitCyclesAssyncron(CyclesPerFloor);
			if (MyInterface != nullptr)
				MyInterface->MovingDown();

			return true;
		}
		break;

	case Core::ES_STOPED_DOOR_OPENED:
		LogMessageInThread(MSG_ERROR, "Can not move down. Dor is opened.");
		return false;
		break;

	case Core::ES_MOVING_UP:
		LogMessageInThread(MSG_ERROR, "Can not move down. Elevator is moving up.");
		return false;
		break;

	case Core::ES_MOVING_DOWN:
		LogMessageInThread(MSG_WARNING, "Already moving down.");
		return true;

	case Core::ES_CLOSING_DOOR:
		LogMessageInThread(MSG_ERROR, "Can not move down. Dor is closing");
		return false;

	case Core::ES_OPENING_DOOR:
		LogMessageInThread(MSG_ERROR, "Can not move down. Dor is opening");
		return false;

	default:
		return false;
		break;
	}
	return false;
}

bool Elevator::OpenDoor()
{
	switch (MyState)
	{
	case Core::ES_STOPED_DOOR_CLOSED:
		SetState(ES_OPENING_DOOR);
		WaitCyclesAssyncron(CyclesPerDoor);
		if (MyInterface != nullptr)
			MyInterface->DoorOpened();
		return true;
		break;

	case Core::ES_STOPED_DOOR_OPENED:
		LogMessageInThread(MSG_WARNING, "Door already opened.");
		return true;
		break;

	case Core::ES_MOVING_UP:
		LogMessageInThread(MSG_ERROR, "Elevator is moving up. Can not open door");
		return false;
		break;

	case Core::ES_MOVING_DOWN:
		LogMessageInThread(MSG_ERROR, "Elevator is moving down. Can not open door");
		return false;

	case Core::ES_CLOSING_DOOR:
		LogMessageInThread(MSG_ERROR, "Can not open the door. Dor is closing");
		return false;

	case Core::ES_OPENING_DOOR:
		LogMessageInThread(MSG_ERROR, "Door is alreadey opening");
		return true;

	default:
		return false;
		break;
	}
	return false;
}

bool Elevator::CloseDoor()
{
	switch (MyState)
	{
	case Core::ES_STOPED_DOOR_CLOSED:
		LogMessageInThread(MSG_ERROR, "Door is alreadey closed");
		return true;

	case Core::ES_STOPED_DOOR_OPENED:
		SetState(ES_CLOSING_DOOR);
		WaitCyclesAssyncron(CyclesPerDoor);
		if (MyInterface != nullptr)
			MyInterface->DoorClosed();

		return true;
		break;

	case Core::ES_MOVING_UP:
		LogMessageInThread(MSG_ERROR, "Elevator is moving up. Can not move the door");
		return false;
		break;
	case Core::ES_MOVING_DOWN:
		LogMessageInThread(MSG_ERROR, "Elevator is moving down. Can not move door");
		return false;

	case Core::ES_CLOSING_DOOR:
		LogMessageInThread(MSG_WARNING, "Door is alreadey closing");
		return true;

	case Core::ES_OPENING_DOOR:
		LogMessageInThread(MSG_ERROR, "Can not close the door. Dor is opening");
		return false;

	default:
		return false;
		break;
	}
	return false;
}

bool Elevator::AddPerson(Person * oPerson)
{
	string msg;

	if (MyPersonList->size() >= Capacity) {
		
		msg = string(oPerson->GetName()) + string(" could not get into the elevator because it has reached its full capacity.");

		LogMessageInThread(MSG_ERROR, msg.c_str());

		return false;
	}

	LockThread();

	MyPersonList->push_back(oPerson);

	UnlockThread();

	if (MyInterface != nullptr)
		MyInterface->AddPerson(oPerson);


	return true;
}

bool Elevator::ReleasePerson(Person * oPerson)
{
	PersonListIterator it;
	string msg;

	it = std::find(MyPersonList->begin(), MyPersonList->end(), oPerson);

	if (it != MyPersonList->end()) {

		LockThread();

		MyPersonList->erase(it);

		UnlockThread();

		return true;

	} else {

		msg = string(oPerson->GetName()) + string(" Is not in the elevator.");

		LogMessageInThread(MSG_ERROR, msg.c_str());

	}

	return false;
}

}  // namespace Core
