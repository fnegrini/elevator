///////////////////////////////////////////////////////////
//  Thread.h
#ifndef CORE_THREAD_H
#define CORE_THREAD_H


#include <windows.h>
#include <string> 
  using std::string;
#include <iostream> 
  using namespace std;

#include "ThreadState.h"
#include "pthread.h"
 
namespace Core
{
class Thread
{

private:
	static pthread_mutex_t _mutex;
	pthread_attr_t _tAttribute;
	pthread_t _threadID;
	bool ThreadCreated;
	Threadstate state;


	void PrintError(const std::string msg);
	virtual void Run();
	void RunInternal();
	static void* RunThread(void* pThread);

protected:
	void JoinThread();
	void StartThread();
	void YieldThread();
	void PauseThread();
	void ResumeThread();
	void StopThread();
	void SetThreadState(Threadstate eState);
	Threadstate GetThreadState();
	virtual void PrintErrorInternal(const std::string msg);

public:
	Thread();
	virtual ~Thread();
	void LockThread();
	void UnlockThread();


};
}
#endif