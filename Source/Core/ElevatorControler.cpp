#include "ElevatorControler.h"
#include "Request.h"
#include "FactoryBase.h"

namespace Core
{

ElevatorControler::ElevatorControler(const char *sName, const char *sElevatorName, LogBase *oLogBase, int iMinFloor, int iMaxFloor, int iCyclesPerFloor, int iCyclesPerDoor, unsigned int iCapacity, FactoryBase *oFactory)
:ComponentBase(sName, oLogBase)
{
	MyFactory = oFactory;

	MyAction = ECA_IDLE;

	MyElevator = new Elevator(sElevatorName, oLogBase, iMinFloor, iMaxFloor, iCyclesPerFloor, iCyclesPerDoor, iCapacity, oFactory);

	if (MyFactory != nullptr)
		MyInterface = MyFactory->CreateElevatorControlerAbstractInterface(this);

}

ElevatorControler::~ElevatorControler()
{
	delete MyElevator;

	if(MyInterface!=nullptr)
		delete MyInterface;
}

void ElevatorControler::StartComponent()
{
	MyElevator->StartComponent();
	Core::ComponentBase::StartComponent();
}

void ElevatorControler::StopComponent()
{
	MyElevator->StopComponent();
	Core::ComponentBase::StopComponent();
}


ElevatorControlerAction ElevatorControler::GetAction()
{
	return MyAction;
}

bool ElevatorControler::GotoFloor(Trip *oTrip, int iFloor)
{
	string msg;
	if (iFloor != MyElevator->GetFloor()) {

		if ((iFloor > MyElevator->GetMaxFloor()) || (iFloor < MyElevator->GetMinFloor())) {
			LogMessageInThread(MSG_ERROR, "Destination Floor is out of range");
			return false;
		}

		msg = string("Controler will move elevator from floor ") + string(to_string(MyElevator->GetFloor())) +
			string(" to floor ") + string(to_string(iFloor));

		LogMessageInThread(MSG_NOTIFICATION, msg.c_str());

		MoveElevatorToFloor(oTrip, iFloor);

		return true;
	} else {

		msg = string("Elevator alreadry on floor ") + string(to_string(iFloor));

		LogMessageInThread(MSG_NOTIFICATION, msg.c_str());

		return true;
	}

	return true;

}

bool ElevatorControler::OpenDoor()
{

	ElevatorOpenDoor();
	
	return true;

}

bool ElevatorControler::CloseDoor()
{

	ElevatorCloseDoor();

	return true;
}

void ElevatorControler::ExecuteTrip(Trip *oTrip)
{
	int Floor;

	LogMessageInThread(MSG_NOTIFICATION, "New Trip started");

	SetAction(ECA_RUNNING_TRIP);

	oTrip->SortFloorsByDirection();

	// Move Elevator to First Floor of trip
	// During this moviment, new requests can be added
	Floor = oTrip->GetNextFloorToGo();

	oTrip->PrepareTrip();

	if (!GotoFloor(oTrip, Floor)) {
		oTrip->EndTrip();
		return;
	}

	oTrip->StartTrip();

	OpenDoor();

	CommitFloorRequests(Floor, oTrip);

	CloseDoor();

	while (!oTrip->TripFinished()) {

		Floor = oTrip->GetNextFloorToGo();

		if (GotoFloor(oTrip, Floor)) {

			if (OpenDoor()) {

				CommitFloorRequests(Floor, oTrip);

				CloseDoor();
			}
		}

	}
	
	oTrip->EndTrip();

	CheckTripSuccess(oTrip);

	SetAction(ECA_IDLE);

	LogMessageInThread(MSG_NOTIFICATION, "Trip finished");

}

Elevator *ElevatorControler::GetElevator()
{
	return MyElevator;
}

Trip * ElevatorControler::CreateTrip(TripDirection eDirection)
{
	Trip *oTrip = new Trip(eDirection, this);

	AddTrip(oTrip);

	return oTrip;

}

Trip * ElevatorControler::GetIdleTrip(TripDirection eDirection)
{
	TripListIterator it;
	Trip *oTrip;
	TripState eState;

	for (it = MyTripList.begin(); it != MyTripList.end(); it++) {

		oTrip = *it;
		eState = oTrip->GetState();
		// 1. Trip not started
		// 2. Same direction
		// 3. Not fullfilled
		if (((eState == TS_BLOCKED)||(eState == TS_RELEASED) ||(eState == TS_PREPARING)) 
			&& (oTrip->GetDirection() == eDirection)
			&& oTrip->GetRequestList()->size() < MyElevator->GetCapacity()) {

			return oTrip;
		}

	}

	return nullptr;
}


bool ElevatorControler::AddTrip(Trip * oTrip)
{

	LockThread();

	MyTripList.push_back(oTrip);

	UnlockThread();

	return true;
}

void ElevatorControler::CheckTripSuccess(Trip * oTrip)
{
	// Check if all requests have finished
	Request *ReqAux;
	RequestList *ReqListAux;
	ReqtListInternaliterator it;
	string msg;
	bool Error = false;

	ReqListAux = oTrip->GetRequestList();
	for (it = ReqListAux->begin(); it != ReqListAux->end(); it++) {

		ReqAux = *it;

		if (ReqAux->GetState() != RQS_DONE) {

			Error = true;

			msg = string("Incomplete move for ") + string(ReqAux->GetPersonName()) +
				string(" to move from floor ") + string(to_string(ReqAux->GetFloorFrom())) +
				string(" to floor ") + string(to_string(ReqAux->GetFloorTo()));

			LogMessageInThread(MSG_ERROR, msg.c_str());
		}

	}

	if (!Error) {
		LogMessageInThread(MSG_NOTIFICATION, "Trip moved all requests to its destination!");
	}


}

string ElevatorControler::ActionToString()
{
	string Result;

	switch (MyAction)
	{
	case Core::ECA_IDLE:
		Result = "Idle";
		break;
	case Core::ECA_RUNNING_TRIP:
		Result = "Running a trip";
		break;
	default:
		Result = "Undefined";
		break;
	}

	return Result;
}

void ElevatorControler::SetAction(ElevatorControlerAction eAction)
{
	string Message;
	
	LockThread();

	MyAction = eAction;

	UnlockThread();

	Message = string("Controler is ") + ActionToString();

	LogMessageInThread(MSG_DEBUG, Message.c_str());

}

bool ElevatorControler::CheckIdle()
{
	if (MyAction == ECA_IDLE) {
		return true;
	}else{
		LogMessageInThread(MSG_ERROR, "Controler is operating. Wait up to be idle");
		return false;
	}
}
void ElevatorControler::Run()
{
	Trip *MyTrip;

	do
	{
		MyTrip = GetNextTrip();

		if (MyTrip != nullptr) {

			ExecuteTrip(MyTrip);

			delete MyTrip;

		}

	} while (MyTrip!=NULL);


}

void ElevatorControler::MoveElevatorToFloor(Trip *oTrip, int Floor)
{

	if (MyElevator->GetState() != ES_STOPED_DOOR_CLOSED) {
		CloseDoor();
	}
	else {
		WaitReleaseElevator();
	}

	while (MyElevator->GetFloor() != Floor) {

		if (MyElevator->GetFloor() > Floor) {
			MyElevator->MoveDown();
		}else {
			MyElevator->MoveUp();
		}

		WaitReleaseElevator();

	}
	
}

void ElevatorControler::ElevatorOpenDoor()
{
	WaitReleaseElevator();
	MyElevator->OpenDoor();
	WaitReleaseElevator();

}

void ElevatorControler::ElevatorCloseDoor()
{
	WaitReleaseElevator();
	MyElevator->CloseDoor();
	WaitReleaseElevator();

}

void ElevatorControler::WaitReleaseElevator()
{

	bool Released = false;
	ElevatorState StateTemp;

	LogMessageInThread(MSG_DEBUG, "Waiting to release elevator");

	while (!Released)
	{
		StateTemp = MyElevator->GetState();
		Released = ((StateTemp == ES_STOPED_DOOR_OPENED) || (StateTemp == ES_STOPED_DOOR_CLOSED));

		if (!Released) {
			WaitUpToOneCycle();
		}

	}

	LogMessageInThread(MSG_DEBUG, "Elevator is released");

}

void ElevatorControler::CommitFloorRequests(int Floor, Trip *oTrip)
{
	std::string msg;

	if (oTrip == nullptr) return;

	msg = std::string("Commiting requests for floor ") + std::string(to_string(Floor));

	LogMessageInThread(MSG_DEBUG, msg.c_str());

	oTrip->CommitFloorRequests(Floor);

}

Trip * ElevatorControler::GetNextTrip()
{
	Trip *Result = nullptr;
	TripListIterator it;

	if (MyTripList.size() == 0)
		return nullptr;

	for (it = MyTripList.begin(); it != MyTripList.end(); it++) {

		Result = *it;

		if (Result->GetState() == TS_RELEASED) {

			LockThread();

			MyTripList.erase(it);

			UnlockThread();

			return Result;
		}
	}

	return nullptr;
}

}  // namespace Core
