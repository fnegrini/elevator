#include <stdio.h>
#include <windows.h>
#include "ComponentBaseCycleThread.h"

namespace Core
{

void ComponentBaseCycleThread::WaitCyclesAssyncron(int iCycleCount)
{
	CycleCount = iCycleCount;
	SetState(CCS_RUNNING);
}

void ComponentBaseCycleThread::DoAfterCycleCountEnds()
{

}

void ComponentBaseCycleThread::SetState(ComponentCycleState eState)
{
	LockThread();
	state = eState;
	UnlockThread();
}


ComponentBaseCycleThread::ComponentBaseCycleThread(const char *sName, LogBase *oLogBase)
:ComponentBase(sName, oLogBase)
{
	CycleCount = 0;
	state = CCS_IDLE;
}

void ComponentBaseCycleThread::Run()
{
	// Infinit Loop Control
	if (state == CCS_IDLE)	return;

	while (CycleCount>0)
	{
		CycleCount--;
		WaitUpToOneCycle();
	}

	SetState(CCS_IDLE);

	DoAfterCycleCountEnds();

}

}  // namespace Core

