#ifndef CORE_REQUEST_H
#define CORE_REQUEST_H


#include "RequestState.h"
#include "Trip.h"

#include <string> 
	using std::string;

namespace Core
{
class Trip;
class Person;

class Request
{
private:
	int FloorFrom;

	int FloorTo;

	int CyclesBeforeStart;

	int CyclesCount;

	int CyclesWaitingOut;

	int CyclesWaitingIn;

	RequestState state;

	Trip *MyTrip;

	Person *MyPerson;

protected:

	virtual void SetState(RequestState eState);

	void WaitUpToOneCycle();

public:
	Request(int iFloorFrom = 0, int iFloorTo = 0, int iCyclesBeforeStart = 0, Person *oPerson = nullptr);

	int GetCyclesWaitingOut();

	int GetCyclesWaitingIn();

	bool GoToTrip(Trip *oTrip, int Floor);

	bool ReleaseTrip(int Floor);

	TripDirection GetDirection();

	virtual void StartRequest();

	int GetFloorFrom();

	int GetFloorTo();

	int GetCyclesBeforeStart();

	RequestState GetState();

	void SimulateJob();

	void IncCyclesWaitingIn();

	void IncCyclesWaitingOut();

	const char *GetPersonName();

	string StateToString();

};

}  // namespace Core
#endif
