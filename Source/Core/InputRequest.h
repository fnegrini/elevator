#ifndef CORE_INPUTREQUEST_H
#define CORE_INPUTREQUEST_H

#include <list>

namespace Core
{

class InputRequest
{
private:
	int FloorFrom;
	int FloorTo;
	int CyclesBeforeStart;

public:
	InputRequest(int iFloorFrom = 0, int iFloorTo = 0, int iCyclesBeforeStart = 0);

	~InputRequest();

	int GetFloorFrom();

	int GetFloorTo();

	int GetCyclesBeforeStart();

	void SetFloorFrom(int iFloorFrom);

	void SetFloorTo(int iFloorTo);

	void SetCyclesBeforeStart(int iCyclesBeforeStart);
};

typedef std::list<InputRequest*> InputRequestList;
typedef std::list<InputRequest*>::iterator InputRequestListIterator;

}

#endif