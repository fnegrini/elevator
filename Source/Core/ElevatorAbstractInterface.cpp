#include "ElevatorAbstractInterface.h"
#include "Elevator.h"

namespace Core {

Elevator * ElevatorAbstractInterface::GetElevator()
{
	return MyElevator;
}

ElevatorAbstractInterface::ElevatorAbstractInterface(Elevator *oElevator)
{
	MyElevator = oElevator;
}


ElevatorAbstractInterface::~ElevatorAbstractInterface()
{
}

}