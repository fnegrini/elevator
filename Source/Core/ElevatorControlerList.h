#ifndef ELEVATOR_CONTROLER_LIST_H
#define ELEVATOR_CONTROLER_LIST_H

#include "MyList.h"
#include "ElevatorControler.h"

namespace Core {

typedef MyList<ElevatorControler*> ElevatorControlerListInternal;

typedef MyList<ElevatorControler*>::MyElement<ElevatorControler*> ElevatorControlerIterator;

class ElevatorControlerList
{

private:

	ElevatorControlerListInternal MyList;

public:

	ElevatorControlerList();

	~ElevatorControlerList();

	ElevatorControlerIterator* GetCurrent(); 

	ElevatorControlerIterator* GetFirst();

	int GetLength();

	void push_back(ElevatorControler* pElement);

	void push_front(ElevatorControler* pElement);

	void clear();

};

}
#endif