#include "Person.h"
#include "ControlerBase.h"

namespace Core
{

void Person::Run()
{
	ReqtListInternaliterator it;
	Request *RequestAux;

	if (MyState == PS_DONE) return;

	if (MyControler == nullptr) {

		LogMessageInThread(MSG_ERROR, "Person has no controler to create request");

	}

	SetState(PS_RUNNING_REQUEST);

	for (it = MyRequestList.begin(); it != MyRequestList.end(); it++) {
		
		RequestAux = *it;
		
		if (RequestAux->GetState() == RQS_IDLE)
			RunRequest(RequestAux);
	}

	SetState(PS_DONE);
}

void Person::RunRequest(Request *oRequest)
{
	ElevatorControler *ElevatorControlerAux;
	string msg;

	oRequest->SimulateJob();

	if (MyControler == nullptr) {
		LogMessageInThread(MSG_ERROR, "Failed to add request to controler");
		return;
	}

	ElevatorControlerAux = MyControler->InsertRequest(oRequest);

	if (ElevatorControlerAux == nullptr) {

		LogMessageInThread(MSG_ERROR, "Failed to determine elevator for request");

		return;

	}

	msg = string("Sucessfuly assigned to controler ") + string(ElevatorControlerAux->GetName()) +
		string("  to go from floor ") + string(to_string(oRequest->GetFloorFrom())) + string(" to floor ") + string(to_string(oRequest->GetFloorTo()));

	LogMessageInThread(MSG_NOTIFICATION, msg.c_str());

	oRequest->StartRequest();

	WaitUpToFinishRequest(oRequest);


}

void Person::WaitUpToFinishRequest(Request *oRequest)
{
	RequestState State;

	State = oRequest->GetState();

	while ((State != RQS_DONE)&&(State != TS_RELEASED)){
		WaitUpToOneCycle();

		switch (State)
		{
		case Core::RQS_WAITING:
			oRequest->IncCyclesWaitingOut();
			break;
		case Core::RQS_ON_TRIP:
			oRequest->IncCyclesWaitingIn();
			break;
		}

		State = oRequest->GetState();
	}


}

string Person::StateToString()
{
	string StateStr;
	
	switch (MyState)
	{
	case Core::PS_IDLE:
		StateStr = "Idle";
		break;

	case Core::PS_RUNNING_REQUEST:
		StateStr = "Running Request";
		break;

	case Core::PS_DONE:
		StateStr = "Done";
		break;

	default:
		StateStr = "Undefined";
		break;
	}

	return StateStr;
}

void Person::SetState(PersonState eState)
{
	string Message;
	string OldStateStr;

	OldStateStr = StateToString();

	LockThread();

	MyState = eState;

	UnlockThread();

	Message = string("State chanded from ") + OldStateStr + string(" to ") + StateToString();

	LogMessageInThread(MSG_DEBUG, Message.c_str());



}

Person::Person(const char *sName, LogBase *oLogBase, ControlerBase *oControler, FactoryBase *oFactory)
:ComponentBase(sName, oLogBase)
{
	MyControler = oControler;
	MyState = PS_IDLE;
	MyFactory = oFactory;

	if (MyFactory != nullptr)
		MyInterface = MyFactory->CreatePersonAbstractInterface(this);

}

Person::~Person()
{
	if(MyInterface!=nullptr)
		delete MyInterface;
}

Request * Person::CreateRequest(int iFloorFrom, int iFloorTo, int iCyclesWait)
{
	Request *RequestAux;
	string reqname;
	
	if (GetThreadState() != THS_IDLE) {

		LogMessageInThread(MSG_ERROR, "Person is walking. Impossible to include new requests");
		return nullptr;
	}

	reqname = string("Request for ") + string(GetName());

	RequestAux = new Request(iFloorFrom, iFloorTo, iCyclesWait, this);

	MyRequestList.InsertRequest(RequestAux);

	return RequestAux;

}
void Person::StopComponent()
{
	SetState(PS_STOPPED);

	ComponentBase::StopComponent();
}

PersonState Person::GetState()
{
	return MyState;
}

void Person::LogStatistics(std::ofstream *File)
{
	ReqtListInternaliterator it;
	Request *RequestAux;
	string msg, status;

	for (it = MyRequestList.begin(); it != MyRequestList.end(); it++) {

		RequestAux = *it;

		if (RequestAux->GetState() != RQS_DONE)
			status = string("Request not finished");
		else
			status = string("Request finished");

		msg = string("From floor ") + string(to_string(RequestAux->GetFloorFrom())) + string(" to floor ") + string(to_string(RequestAux->GetFloorTo())) +
			string(" - ") + status + string(": ") +
			string(to_string(RequestAux->GetCyclesWaitingOut())) + string(" cycles waiting out of elevador; ") +
			string(to_string(RequestAux->GetCyclesWaitingIn())) + string(" cycles waiting inside elevator; ");

		LogMessageInternal(MSG_NOTIFICATION, msg.c_str());

		msg = string(GetName()) + "," +
			string(to_string(RequestAux->GetFloorFrom())) + string(",") +
			string(to_string(RequestAux->GetFloorTo())) + string(",") +
			string(RequestAux->StateToString()) + string(",") +
			string(to_string(RequestAux->GetCyclesWaitingOut())) + string(",") +
			string(to_string(RequestAux->GetCyclesWaitingIn())) + string("\n");

		*File << msg.c_str();
	}
}


}