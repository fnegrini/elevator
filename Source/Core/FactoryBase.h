#ifndef CORE_FACTORY_BASE_H
#define CORE_FACTORY_BASE_H

#include "OutputType.h"
#include "ElevatorAbstractInterface.h"
#include "PersonAbstractInterface.h"
#include "ElevatorControlerAbstractInterface.h"
#include "ControlerType.h"

namespace Core {

class LogBase;
class InputPersonBase;
class Elevator;
class Person;
class ElevatorControler;
class ControlerBase;

class FactoryBase
{

public:
	FactoryBase();

	static FactoryBase *GetFactory(OutputType eOutput = OT_STREAM);

	virtual ElevatorAbstractInterface *CreateElevatorAbstractInterface(Elevator *oElevator) abstract;

	virtual ElevatorControlerAbstractInterface *CreateElevatorControlerAbstractInterface(ElevatorControler *oElevatorControler) abstract;

	virtual PersonAbstractInterface *CreatePersonAbstractInterface(Person *oPerson) abstract;

	virtual LogBase *CreateLog() abstract;

	virtual InputPersonBase *CreateInputPerson(const char *sName = "", LogBase *oLogBase = nullptr, ControlerBase *oControlerBase = nullptr) abstract;

	virtual void SetupEnvironmentInteractive(ControlerBase *oControler) abstract;

	virtual ControlerType GetControlerTypeInteractive() abstract;

};

}

#endif

