#ifndef CORE_PERSONTSTATE_H
#define CORE_PERSONTSTATE_H

#include <list>

namespace Core
{
	enum PersonState
	{
		PS_IDLE,
		PS_RUNNING_REQUEST,
		PS_DONE,
		PS_STOPPED,
	};

}

#endif
