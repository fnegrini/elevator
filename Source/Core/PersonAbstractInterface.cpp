#include "PersonAbstractInterface.h"
#include "Person.h"

namespace Core {

Person * PersonAbstractInterface::GetPerson()
{
	return MyPerson;
}

PersonAbstractInterface::PersonAbstractInterface(Person *oPerson)
{
	MyPerson = oPerson;
}


PersonAbstractInterface::~PersonAbstractInterface()
{
}

}