#include <stdio.h>
#include <windows.h>
#include "ComponentBase.h"

namespace Core
{
long ComponentBase::CycleInterval = 1000;


void ComponentBase::LogMessageInThread(MessageType Type, const char *Message)
{
	LockThread();
	LogMessageInternal(Type, Message);
	UnlockThread();
}

void ComponentBase::WaitUpToOneCycle()
{
	Sleep(CycleInterval);
}

ComponentBase::ComponentBase(const char *sName, LogBase *oLogBase)
:LoggerBase(sName, oLogBase)
{

}

ComponentBase::~ComponentBase()
{
	if (GetThreadState() == THS_RUNNING) {

		JoinThread();
	}
}


void ComponentBase::SetCycleInterval(long sCycleInterval)
{
	CycleInterval = sCycleInterval;
}

long ComponentBase::GetCycleInterval()
{
	return CycleInterval;
}


void ComponentBase::StartComponent()
{
	StartThread();
}

void ComponentBase::StopComponent()
{
	StopThread();
}


void ComponentBase::PrintErrorInternal(const string msg)
{
	LogMessageInternal(MSG_ERROR, (char *) msg.c_str());
}


}  // namespace Core

