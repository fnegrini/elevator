#include "LogBase.h"
#include "LoggerBase.h"

#include <time.h>
#include <iostream>

namespace Core
{
std::string LogBase::MessageTypeToString(MessageType Type)
{
	std::string TypeStr;
	switch (Type)
	{
	case Core::MSG_UNDEFINED:
		TypeStr = "Undefined";
		break;

	case Core::MSG_NOTIFICATION:
		TypeStr = "Notification";
		break;

	case Core::MSG_ERROR:
		TypeStr = "Error";
		break;

	case Core::MSG_WARNING:
		TypeStr = "Warning";
		break;

	case Core::MSG_DEBUG:
		TypeStr = "Debug";
		break;

	default:
		TypeStr = "Untyped";
		break;
	}
	return TypeStr;
}
void LogBase::LogMessageInternal(LoggerBase *oLoggerBase, MessageType Type, const char * Message)
{

}
bool LogBase::MessageTypeActive(MessageType eType)
{

	return (std::find(activeTypes.begin(), activeTypes.end(), eType) != activeTypes.end());;
}

std::string LogBase::GetTimeStamp()
{
	
	std::string TimeStr = "";
	
	time_t rawtime;
	struct tm * ptm;

	time(&rawtime);

	ptm = gmtime(&rawtime);

	TimeStr = asctime(ptm);

	TimeStr = std::string(to_string(ptm->tm_year + 1900)) + std::string("-") +
	std::string(to_string(ptm->tm_mon)) + std::string("-") +
	std::string(to_string(ptm->tm_mday)) + std::string(" ") +
	std::string(to_string(ptm->tm_hour)) + std::string(":") +
	std::string(to_string(ptm->tm_min)) + std::string(":") +
	std::string(to_string(ptm->tm_sec));
	
	return TimeStr;

}

LogBase::LogBase()
{
	activeTypes = { MSG_UNDEFINED, MSG_NOTIFICATION, MSG_ERROR, MSG_WARNING, MSG_DEBUG };

}
void LogBase::LogMessage(LoggerBase *oLoggerBase, MessageType eType, const char * sMessage)
{
	if (MessageTypeActive(eType)) {

		LogMessageInternal(oLoggerBase, eType, sMessage);

	}
}

MessageTypeList LogBase::GetActiveTypes()
{
	return activeTypes;
}

void LogBase::EnableMessageType(MessageType eType)
{
	activeTypes.merge({ eType });
}

void LogBase::DisableMessageType(MessageType eType)
{
	activeTypes.remove(eType);
}

}  // namespace Core
