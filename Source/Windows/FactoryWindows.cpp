#include "FactoryWindows.h"
#include "../Console/InputPersonFile.h"
#include "../Console/LogStream.h"
#include "ElevatorInterfaceWindows.h"


namespace Windows {

const char FILE_NAME[] = "Requests.csv";

FactoryWindows::FactoryWindows()
{
}

FactoryWindows::~FactoryWindows()
{
}

Core::ElevatorAbstractInterface * FactoryWindows::CreateElevatorAbstractInterface(Core::Elevator * oElevator)
{
	return new ElevatorInterfaceWindows(oElevator);
}
Core::ElevatorControlerAbstractInterface * FactoryWindows::CreateElevatorControlerAbstractInterface(Core::ElevatorControler * oElevatorControler)
{
	return nullptr;
}
Core::PersonAbstractInterface * FactoryWindows::CreatePersonAbstractInterface(Core::Person * oPerson)
{
	return nullptr;
}
Core::LogBase * FactoryWindows::CreateLog()
{
	return new Console::LogStream();
}
Core::InputPersonBase * FactoryWindows::CreateInputPerson(const char * sName, Core::LogBase * oLogBase, Core::ControlerBase * oControlerBase)
{
	return new Console::InputPersonFile(sName, oLogBase, oControlerBase, this, FILE_NAME);
}

void FactoryWindows::SetupEnvironmentInteractive(Core::ControlerBase * oControler)
{
}

Core::ControlerType FactoryWindows::GetControlerTypeInteractive()
{
	return Core::CT_STANDARD;
}

}