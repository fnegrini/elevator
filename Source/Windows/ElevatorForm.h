#ifndef WINDOWS_ELEVATOR_CONSOLE_H
#define WINDOWS_ELEVATOR_CONSOLE_H

namespace ElevatorConsole {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	typedef System::Windows::Forms::TrackBar^ ElevatorWindows;

	/// <summary>
	/// Summary for ElevatorForm
	/// </summary>
	public ref class ElevatorForm : public System::Windows::Forms::Form
	{
	public:
		ElevatorForm(void)
		{
			InitializeComponent();
			//
			ElevatorCount = 0;
			//
		}
		
		ElevatorWindows GetNextElevator() {
			ElevatorCount++;

			switch (ElevatorCount)
			{
			case 1:
				return Elevator1;
				break;
			case 2:
				return Elevator2;
				break;
			case 3:
				return Elevator3;
				break;
			case 4:
				return Elevator4;
				break;
			case 5:
				return Elevator5;
				break;
			case 6:
				return Elevator6;
				break;
			case 7:
				return Elevator7;
				break;
			case 8:
				return Elevator8;
				break;
			case 9:
				return Elevator9;
				break;
			case 10:
				return Elevator10;
				break;
			default:
				return nullptr;
				break;
			}
		}
	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~ElevatorForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::TabControl^  tcElevator;
	protected:
	private: System::Windows::Forms::TabPage^  tpElevators;
	private: System::Windows::Forms::TabPage^  tpRequests;
	private: System::Windows::Forms::GroupBox^  gbElevator1;
	private: System::Windows::Forms::TrackBar^  Elevator1;
	private: System::Windows::Forms::GroupBox^  gbElevator2;
	private: System::Windows::Forms::TrackBar^  Elevator2;
	private: System::Windows::Forms::GroupBox^  gbElevator3;
	private: System::Windows::Forms::TrackBar^  Elevator3;
	private: System::Windows::Forms::GroupBox^  gbElevator4;
	private: System::Windows::Forms::TrackBar^  Elevator4;
	private: System::Windows::Forms::GroupBox^  gbElevator6;
	private: System::Windows::Forms::TrackBar^  Elevator6;
	private: System::Windows::Forms::GroupBox^  gbElevator5;
	private: System::Windows::Forms::TrackBar^  Elevator5;
	private: System::Windows::Forms::GroupBox^  gbElevator10;
	private: System::Windows::Forms::TrackBar^  Elevator10;
	private: System::Windows::Forms::GroupBox^  gbElevator9;
	private: System::Windows::Forms::TrackBar^  Elevator9;
	private: System::Windows::Forms::GroupBox^  gbElevator8;
	private: System::Windows::Forms::TrackBar^  Elevator8;

	private: System::Windows::Forms::GroupBox^  gbElevator7;
	private: System::Windows::Forms::TrackBar^  Elevator7;

	int ElevatorCount;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->tcElevator = (gcnew System::Windows::Forms::TabControl());
			this->tpElevators = (gcnew System::Windows::Forms::TabPage());
			this->gbElevator10 = (gcnew System::Windows::Forms::GroupBox());
			this->Elevator10 = (gcnew System::Windows::Forms::TrackBar());
			this->gbElevator9 = (gcnew System::Windows::Forms::GroupBox());
			this->Elevator9 = (gcnew System::Windows::Forms::TrackBar());
			this->gbElevator8 = (gcnew System::Windows::Forms::GroupBox());
			this->Elevator8 = (gcnew System::Windows::Forms::TrackBar());
			this->gbElevator7 = (gcnew System::Windows::Forms::GroupBox());
			this->Elevator7 = (gcnew System::Windows::Forms::TrackBar());
			this->gbElevator6 = (gcnew System::Windows::Forms::GroupBox());
			this->Elevator6 = (gcnew System::Windows::Forms::TrackBar());
			this->gbElevator5 = (gcnew System::Windows::Forms::GroupBox());
			this->Elevator5 = (gcnew System::Windows::Forms::TrackBar());
			this->gbElevator4 = (gcnew System::Windows::Forms::GroupBox());
			this->Elevator4 = (gcnew System::Windows::Forms::TrackBar());
			this->gbElevator3 = (gcnew System::Windows::Forms::GroupBox());
			this->Elevator3 = (gcnew System::Windows::Forms::TrackBar());
			this->gbElevator2 = (gcnew System::Windows::Forms::GroupBox());
			this->Elevator2 = (gcnew System::Windows::Forms::TrackBar());
			this->gbElevator1 = (gcnew System::Windows::Forms::GroupBox());
			this->Elevator1 = (gcnew System::Windows::Forms::TrackBar());
			this->tpRequests = (gcnew System::Windows::Forms::TabPage());
			this->tcElevator->SuspendLayout();
			this->tpElevators->SuspendLayout();
			this->gbElevator10->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Elevator10))->BeginInit();
			this->gbElevator9->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Elevator9))->BeginInit();
			this->gbElevator8->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Elevator8))->BeginInit();
			this->gbElevator7->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Elevator7))->BeginInit();
			this->gbElevator6->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Elevator6))->BeginInit();
			this->gbElevator5->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Elevator5))->BeginInit();
			this->gbElevator4->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Elevator4))->BeginInit();
			this->gbElevator3->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Elevator3))->BeginInit();
			this->gbElevator2->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Elevator2))->BeginInit();
			this->gbElevator1->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Elevator1))->BeginInit();
			this->SuspendLayout();
			// 
			// tcElevator
			// 
			this->tcElevator->Controls->Add(this->tpElevators);
			this->tcElevator->Controls->Add(this->tpRequests);
			this->tcElevator->Dock = System::Windows::Forms::DockStyle::Fill;
			this->tcElevator->Location = System::Drawing::Point(0, 0);
			this->tcElevator->Name = L"tcElevator";
			this->tcElevator->SelectedIndex = 0;
			this->tcElevator->Size = System::Drawing::Size(552, 261);
			this->tcElevator->TabIndex = 0;
			// 
			// tpElevators
			// 
			this->tpElevators->Controls->Add(this->gbElevator10);
			this->tpElevators->Controls->Add(this->gbElevator9);
			this->tpElevators->Controls->Add(this->gbElevator8);
			this->tpElevators->Controls->Add(this->gbElevator7);
			this->tpElevators->Controls->Add(this->gbElevator6);
			this->tpElevators->Controls->Add(this->gbElevator5);
			this->tpElevators->Controls->Add(this->gbElevator4);
			this->tpElevators->Controls->Add(this->gbElevator3);
			this->tpElevators->Controls->Add(this->gbElevator2);
			this->tpElevators->Controls->Add(this->gbElevator1);
			this->tpElevators->Location = System::Drawing::Point(4, 22);
			this->tpElevators->Name = L"tpElevators";
			this->tpElevators->Padding = System::Windows::Forms::Padding(3);
			this->tpElevators->Size = System::Drawing::Size(544, 235);
			this->tpElevators->TabIndex = 0;
			this->tpElevators->Text = L"Elevators";
			this->tpElevators->UseVisualStyleBackColor = true;
			// 
			// gbElevator10
			// 
			this->gbElevator10->Controls->Add(this->Elevator10);
			this->gbElevator10->Dock = System::Windows::Forms::DockStyle::Left;
			this->gbElevator10->Location = System::Drawing::Point(480, 3);
			this->gbElevator10->Name = L"gbElevator10";
			this->gbElevator10->RightToLeft = System::Windows::Forms::RightToLeft::Yes;
			this->gbElevator10->Size = System::Drawing::Size(53, 229);
			this->gbElevator10->TabIndex = 10;
			this->gbElevator10->TabStop = false;
			this->gbElevator10->Text = L"10";
			// 
			// Elevator10
			// 
			this->Elevator10->AccessibleDescription = L"Elevator 1";
			this->Elevator10->Dock = System::Windows::Forms::DockStyle::Left;
			this->Elevator10->Enabled = false;
			this->Elevator10->Location = System::Drawing::Point(3, 16);
			this->Elevator10->Name = L"Elevator10";
			this->Elevator10->Orientation = System::Windows::Forms::Orientation::Vertical;
			this->Elevator10->Size = System::Drawing::Size(45, 210);
			this->Elevator10->TabIndex = 1;
			this->Elevator10->TickStyle = System::Windows::Forms::TickStyle::Both;
			// 
			// gbElevator9
			// 
			this->gbElevator9->Controls->Add(this->Elevator9);
			this->gbElevator9->Dock = System::Windows::Forms::DockStyle::Left;
			this->gbElevator9->Location = System::Drawing::Point(427, 3);
			this->gbElevator9->Name = L"gbElevator9";
			this->gbElevator9->RightToLeft = System::Windows::Forms::RightToLeft::Yes;
			this->gbElevator9->Size = System::Drawing::Size(53, 229);
			this->gbElevator9->TabIndex = 9;
			this->gbElevator9->TabStop = false;
			this->gbElevator9->Text = L"09";
			// 
			// Elevator9
			// 
			this->Elevator9->AccessibleDescription = L"Elevator 1";
			this->Elevator9->Dock = System::Windows::Forms::DockStyle::Left;
			this->Elevator9->Enabled = false;
			this->Elevator9->Location = System::Drawing::Point(3, 16);
			this->Elevator9->Name = L"Elevator9";
			this->Elevator9->Orientation = System::Windows::Forms::Orientation::Vertical;
			this->Elevator9->Size = System::Drawing::Size(45, 210);
			this->Elevator9->TabIndex = 1;
			this->Elevator9->TickStyle = System::Windows::Forms::TickStyle::Both;
			// 
			// gbElevator8
			// 
			this->gbElevator8->Controls->Add(this->Elevator8);
			this->gbElevator8->Dock = System::Windows::Forms::DockStyle::Left;
			this->gbElevator8->Location = System::Drawing::Point(374, 3);
			this->gbElevator8->Name = L"gbElevator8";
			this->gbElevator8->RightToLeft = System::Windows::Forms::RightToLeft::Yes;
			this->gbElevator8->Size = System::Drawing::Size(53, 229);
			this->gbElevator8->TabIndex = 8;
			this->gbElevator8->TabStop = false;
			this->gbElevator8->Text = L"08";
			// 
			// Elevator8
			// 
			this->Elevator8->AccessibleDescription = L"Elevator 1";
			this->Elevator8->Dock = System::Windows::Forms::DockStyle::Left;
			this->Elevator8->Enabled = false;
			this->Elevator8->Location = System::Drawing::Point(3, 16);
			this->Elevator8->Name = L"Elevator8";
			this->Elevator8->Orientation = System::Windows::Forms::Orientation::Vertical;
			this->Elevator8->Size = System::Drawing::Size(45, 210);
			this->Elevator8->TabIndex = 1;
			this->Elevator8->TickStyle = System::Windows::Forms::TickStyle::Both;
			// 
			// gbElevator7
			// 
			this->gbElevator7->Controls->Add(this->Elevator7);
			this->gbElevator7->Dock = System::Windows::Forms::DockStyle::Left;
			this->gbElevator7->Location = System::Drawing::Point(321, 3);
			this->gbElevator7->Name = L"gbElevator7";
			this->gbElevator7->RightToLeft = System::Windows::Forms::RightToLeft::Yes;
			this->gbElevator7->Size = System::Drawing::Size(53, 229);
			this->gbElevator7->TabIndex = 7;
			this->gbElevator7->TabStop = false;
			this->gbElevator7->Text = L"07";
			// 
			// Elevator7
			// 
			this->Elevator7->AccessibleDescription = L"Elevator 1";
			this->Elevator7->Dock = System::Windows::Forms::DockStyle::Left;
			this->Elevator7->Enabled = false;
			this->Elevator7->Location = System::Drawing::Point(3, 16);
			this->Elevator7->Name = L"Elevator7";
			this->Elevator7->Orientation = System::Windows::Forms::Orientation::Vertical;
			this->Elevator7->Size = System::Drawing::Size(45, 210);
			this->Elevator7->TabIndex = 1;
			this->Elevator7->TickStyle = System::Windows::Forms::TickStyle::Both;
			// 
			// gbElevator6
			// 
			this->gbElevator6->Controls->Add(this->Elevator6);
			this->gbElevator6->Dock = System::Windows::Forms::DockStyle::Left;
			this->gbElevator6->Location = System::Drawing::Point(268, 3);
			this->gbElevator6->Name = L"gbElevator6";
			this->gbElevator6->RightToLeft = System::Windows::Forms::RightToLeft::Yes;
			this->gbElevator6->Size = System::Drawing::Size(53, 229);
			this->gbElevator6->TabIndex = 6;
			this->gbElevator6->TabStop = false;
			this->gbElevator6->Text = L"06";
			// 
			// Elevator6
			// 
			this->Elevator6->AccessibleDescription = L"Elevator 1";
			this->Elevator6->Dock = System::Windows::Forms::DockStyle::Left;
			this->Elevator6->Enabled = false;
			this->Elevator6->Location = System::Drawing::Point(3, 16);
			this->Elevator6->Name = L"Elevator6";
			this->Elevator6->Orientation = System::Windows::Forms::Orientation::Vertical;
			this->Elevator6->Size = System::Drawing::Size(45, 210);
			this->Elevator6->TabIndex = 1;
			this->Elevator6->TickStyle = System::Windows::Forms::TickStyle::Both;
			// 
			// gbElevator5
			// 
			this->gbElevator5->Controls->Add(this->Elevator5);
			this->gbElevator5->Dock = System::Windows::Forms::DockStyle::Left;
			this->gbElevator5->Location = System::Drawing::Point(215, 3);
			this->gbElevator5->Name = L"gbElevator5";
			this->gbElevator5->RightToLeft = System::Windows::Forms::RightToLeft::Yes;
			this->gbElevator5->Size = System::Drawing::Size(53, 229);
			this->gbElevator5->TabIndex = 5;
			this->gbElevator5->TabStop = false;
			this->gbElevator5->Text = L"05";
			// 
			// Elevator5
			// 
			this->Elevator5->AccessibleDescription = L"Elevator 1";
			this->Elevator5->Dock = System::Windows::Forms::DockStyle::Left;
			this->Elevator5->Enabled = false;
			this->Elevator5->Location = System::Drawing::Point(3, 16);
			this->Elevator5->Name = L"Elevator5";
			this->Elevator5->Orientation = System::Windows::Forms::Orientation::Vertical;
			this->Elevator5->Size = System::Drawing::Size(45, 210);
			this->Elevator5->TabIndex = 1;
			this->Elevator5->TickStyle = System::Windows::Forms::TickStyle::Both;
			// 
			// gbElevator4
			// 
			this->gbElevator4->Controls->Add(this->Elevator4);
			this->gbElevator4->Dock = System::Windows::Forms::DockStyle::Left;
			this->gbElevator4->Location = System::Drawing::Point(162, 3);
			this->gbElevator4->Name = L"gbElevator4";
			this->gbElevator4->RightToLeft = System::Windows::Forms::RightToLeft::Yes;
			this->gbElevator4->Size = System::Drawing::Size(53, 229);
			this->gbElevator4->TabIndex = 4;
			this->gbElevator4->TabStop = false;
			this->gbElevator4->Text = L"04";
			// 
			// Elevator4
			// 
			this->Elevator4->AccessibleDescription = L"Elevator 1";
			this->Elevator4->Dock = System::Windows::Forms::DockStyle::Left;
			this->Elevator4->Enabled = false;
			this->Elevator4->Location = System::Drawing::Point(3, 16);
			this->Elevator4->Name = L"Elevator4";
			this->Elevator4->Orientation = System::Windows::Forms::Orientation::Vertical;
			this->Elevator4->Size = System::Drawing::Size(45, 210);
			this->Elevator4->TabIndex = 1;
			this->Elevator4->TickStyle = System::Windows::Forms::TickStyle::Both;
			// 
			// gbElevator3
			// 
			this->gbElevator3->Controls->Add(this->Elevator3);
			this->gbElevator3->Dock = System::Windows::Forms::DockStyle::Left;
			this->gbElevator3->Location = System::Drawing::Point(109, 3);
			this->gbElevator3->Name = L"gbElevator3";
			this->gbElevator3->RightToLeft = System::Windows::Forms::RightToLeft::Yes;
			this->gbElevator3->Size = System::Drawing::Size(53, 229);
			this->gbElevator3->TabIndex = 3;
			this->gbElevator3->TabStop = false;
			this->gbElevator3->Text = L"03";
			// 
			// Elevator3
			// 
			this->Elevator3->AccessibleDescription = L"Elevator 1";
			this->Elevator3->Dock = System::Windows::Forms::DockStyle::Left;
			this->Elevator3->Enabled = false;
			this->Elevator3->Location = System::Drawing::Point(3, 16);
			this->Elevator3->Name = L"Elevator3";
			this->Elevator3->Orientation = System::Windows::Forms::Orientation::Vertical;
			this->Elevator3->Size = System::Drawing::Size(45, 210);
			this->Elevator3->TabIndex = 1;
			this->Elevator3->TickStyle = System::Windows::Forms::TickStyle::Both;
			// 
			// gbElevator2
			// 
			this->gbElevator2->Controls->Add(this->Elevator2);
			this->gbElevator2->Dock = System::Windows::Forms::DockStyle::Left;
			this->gbElevator2->Location = System::Drawing::Point(56, 3);
			this->gbElevator2->Name = L"gbElevator2";
			this->gbElevator2->RightToLeft = System::Windows::Forms::RightToLeft::Yes;
			this->gbElevator2->Size = System::Drawing::Size(53, 229);
			this->gbElevator2->TabIndex = 2;
			this->gbElevator2->TabStop = false;
			this->gbElevator2->Text = L"02";
			// 
			// Elevator2
			// 
			this->Elevator2->AccessibleDescription = L"Elevator 1";
			this->Elevator2->Dock = System::Windows::Forms::DockStyle::Left;
			this->Elevator2->Enabled = false;
			this->Elevator2->Location = System::Drawing::Point(3, 16);
			this->Elevator2->Name = L"Elevator2";
			this->Elevator2->Orientation = System::Windows::Forms::Orientation::Vertical;
			this->Elevator2->Size = System::Drawing::Size(45, 210);
			this->Elevator2->TabIndex = 1;
			this->Elevator2->TickStyle = System::Windows::Forms::TickStyle::Both;
			// 
			// gbElevator1
			// 
			this->gbElevator1->Controls->Add(this->Elevator1);
			this->gbElevator1->Dock = System::Windows::Forms::DockStyle::Left;
			this->gbElevator1->Location = System::Drawing::Point(3, 3);
			this->gbElevator1->Name = L"gbElevator1";
			this->gbElevator1->RightToLeft = System::Windows::Forms::RightToLeft::Yes;
			this->gbElevator1->Size = System::Drawing::Size(53, 229);
			this->gbElevator1->TabIndex = 1;
			this->gbElevator1->TabStop = false;
			this->gbElevator1->Text = L"01";
			// 
			// Elevator1
			// 
			this->Elevator1->AccessibleDescription = L"Elevator 1";
			this->Elevator1->Dock = System::Windows::Forms::DockStyle::Left;
			this->Elevator1->Enabled = false;
			this->Elevator1->Location = System::Drawing::Point(3, 16);
			this->Elevator1->Name = L"Elevator1";
			this->Elevator1->Orientation = System::Windows::Forms::Orientation::Vertical;
			this->Elevator1->Size = System::Drawing::Size(45, 210);
			this->Elevator1->TabIndex = 1;
			this->Elevator1->TickStyle = System::Windows::Forms::TickStyle::Both;
			// 
			// tpRequests
			// 
			this->tpRequests->Location = System::Drawing::Point(4, 22);
			this->tpRequests->Name = L"tpRequests";
			this->tpRequests->Padding = System::Windows::Forms::Padding(3);
			this->tpRequests->Size = System::Drawing::Size(544, 235);
			this->tpRequests->TabIndex = 1;
			this->tpRequests->Text = L"Requests";
			this->tpRequests->UseVisualStyleBackColor = true;
			// 
			// ElevatorForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(552, 261);
			this->Controls->Add(this->tcElevator);
			this->Name = L"ElevatorForm";
			this->Text = L"ElevatorForm";
			this->tcElevator->ResumeLayout(false);
			this->tpElevators->ResumeLayout(false);
			this->gbElevator10->ResumeLayout(false);
			this->gbElevator10->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Elevator10))->EndInit();
			this->gbElevator9->ResumeLayout(false);
			this->gbElevator9->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Elevator9))->EndInit();
			this->gbElevator8->ResumeLayout(false);
			this->gbElevator8->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Elevator8))->EndInit();
			this->gbElevator7->ResumeLayout(false);
			this->gbElevator7->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Elevator7))->EndInit();
			this->gbElevator6->ResumeLayout(false);
			this->gbElevator6->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Elevator6))->EndInit();
			this->gbElevator5->ResumeLayout(false);
			this->gbElevator5->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Elevator5))->EndInit();
			this->gbElevator4->ResumeLayout(false);
			this->gbElevator4->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Elevator4))->EndInit();
			this->gbElevator3->ResumeLayout(false);
			this->gbElevator3->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Elevator3))->EndInit();
			this->gbElevator2->ResumeLayout(false);
			this->gbElevator2->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Elevator2))->EndInit();
			this->gbElevator1->ResumeLayout(false);
			this->gbElevator1->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Elevator1))->EndInit();
			this->ResumeLayout(false);

		}
#pragma endregion
	};
}

#endif