#include "ElevatorInterfaceWindows.h"
#include "../Core/Elevator.h"
#include "../Core/Person.h"

namespace Windows {



ElevatorInterfaceWindows::ElevatorInterfaceWindows(Core::Elevator *oElevator)
:Core::ElevatorAbstractInterface(oElevator)
{

}

void ElevatorInterfaceWindows::MovingUp()
{

}

void ElevatorInterfaceWindows::MovingDown()
{

}

void ElevatorInterfaceWindows::ArrivedToFloor(int Floor)
{

}

void ElevatorInterfaceWindows::DoorOpened()
{

}

void ElevatorInterfaceWindows::DoorClosed()
{
}

void ElevatorInterfaceWindows::AddPerson(Core::Person * oPerson)
{
}

}