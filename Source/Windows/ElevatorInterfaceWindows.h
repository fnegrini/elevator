#ifndef WINDOWS_ELEVATOR_WINDOWS_H
#define WINDOWS_ELEVATOR_WINDOWS_H

#include <vcclr.h>

#include "../Core/ElevatorAbstractInterface.h"

namespace Windows {

class ElevatorInterfaceWindows : public Core::ElevatorAbstractInterface
{
private:

public:

	ElevatorInterfaceWindows(Core::Elevator *oElevator = nullptr);

	virtual void MovingUp();

	virtual void MovingDown();

	virtual void ArrivedToFloor(int Floor);

	virtual void DoorOpened();

	virtual void DoorClosed();

	virtual void AddPerson(Core::Person *oPerson);
};

}

#endif