#ifndef WINDOWS_FACTORY_WINDOWS_H
#define WINDOWS_FACTORY_WINDOWS_H

#include <vcclr.h>

#include "../Core/FactoryBase.h"
#include "../Core/Thread.h"
#include "ElevatorForm.h"

namespace Windows {



class FactoryWindows :
	public Core::FactoryBase
{
private:


public:

	FactoryWindows();

	~FactoryWindows();

	virtual Core::ElevatorAbstractInterface *CreateElevatorAbstractInterface(Core::Elevator *oElevator);

	virtual Core::ElevatorControlerAbstractInterface *CreateElevatorControlerAbstractInterface(Core::ElevatorControler *oElevatorControler);

	virtual Core::PersonAbstractInterface *CreatePersonAbstractInterface(Core::Person *oPerson);

	virtual Core::LogBase *CreateLog();

	virtual Core::InputPersonBase *CreateInputPerson(const char *sName = "", Core::LogBase *oLogBase = nullptr, Core::ControlerBase *oControlerBase = nullptr);

	virtual void SetupEnvironmentInteractive(Core::ControlerBase *oControler);

	virtual Core::ControlerType GetControlerTypeInteractive();

};

}
#endif

