#ifndef CONTROLER_CONTROLER_S_UP_S_DOWN_H
#define CONTROLER_CONTROLER_S_UP_S_DOWN_H

#include "../Core/ControlerBase.h"

namespace Controler
{
class ControlerSUpSDown : public Core::ControlerBase
{

protected:

	virtual Core::Trip *DetermineControlerToRequest(Core::Request *oRequest);

public:

	ControlerSUpSDown(const char *sName = "", Core::LogBase *oLogBase = nullptr, Core::FactoryBase *oFactory = nullptr);
};

}  // namespace Controler
#endif
