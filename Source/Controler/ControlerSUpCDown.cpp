#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ControlerSUpCDown.h"

namespace Controler
{

Core::Trip * ControlerSUpCDown::DetermineControlerToRequest(Core::Request * oRequest)
{
	return Core::ControlerBase::DetermineControlerToRequest(oRequest);
}

ControlerSUpCDown::ControlerSUpCDown(const char * sName, Core::LogBase * oLogBase, Core::FactoryBase * oFactory)
:Core::ControlerBase(sName, oLogBase, oFactory)
{
}

}  // namespace Controler
