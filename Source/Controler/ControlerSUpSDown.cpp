#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ControlerSUpSDown.h"

namespace Controler
{

Core::Trip * ControlerSUpSDown::DetermineControlerToRequest(Core::Request * oRequest)
{
	return Core::ControlerBase::DetermineControlerToRequest(oRequest);
}

ControlerSUpSDown::ControlerSUpSDown(const char * sName, Core::LogBase * oLogBase, Core::FactoryBase * oFactory)
:Core::ControlerBase(sName, oLogBase, oFactory)
{
}

}  // namespace Controler
