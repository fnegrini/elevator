#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ControlerCUpSDown.h"

namespace Controler
{

Core::Trip * ControlerCUpSDown::DetermineControlerToRequest(Core::Request * oRequest)
{
	return Core::ControlerBase::DetermineControlerToRequest(oRequest);
}

ControlerCUpSDown::ControlerCUpSDown(const char * sName, Core::LogBase * oLogBase, Core::FactoryBase * oFactory)
:Core::ControlerBase(sName, oLogBase, oFactory)
{
}

}  // namespace Controler
