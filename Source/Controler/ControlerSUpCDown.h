#ifndef CONTROLER_CONTROLER_S_UP_C_DOWN_H
#define CONTROLER_CONTROLER_S_UP_C_DOWN_H

#include "../Core/ControlerBase.h"

namespace Controler
{
class ControlerSUpCDown : public Core::ControlerBase
{

protected:

	virtual Core::Trip *DetermineControlerToRequest(Core::Request *oRequest);

public:
	ControlerSUpCDown(const char *sName = "", Core::LogBase *oLogBase = nullptr, Core::FactoryBase *oFactory = nullptr);
};

}  // namespace Controler
#endif
