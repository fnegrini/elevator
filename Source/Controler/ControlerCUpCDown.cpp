#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ControlerCUpCDown.h"

namespace Controler
{

Core::Trip * ControlerCUpCDown::DetermineControlerToRequest(Core::Request * oRequest)
{
	return Core::ControlerBase::DetermineControlerToRequest(oRequest);
}

ControlerCUpCDown::ControlerCUpCDown(const char * sName, Core::LogBase * oLogBase, Core::FactoryBase * oFactory)
:Core::ControlerBase(sName, oLogBase, oFactory)
{
}

}  // namespace Controler
