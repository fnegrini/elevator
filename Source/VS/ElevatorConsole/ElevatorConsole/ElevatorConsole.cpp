// ElevatorConsole.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "../../../Core/Main.h"
int main()
{

	Core::Main Main = Core::Main(1000, Core::OT_STREAM);

	Main.GetLog()->DisableMessageType(Core::MSG_DEBUG);

	Main.Setup();

	cout << "\nPress ENTER to start simulation\n";

	getchar();

	getchar();

	Main.Execute();

	getchar();

	Main.Stop();

	Main.LogStatistics();

	getchar();

	return 0;
}

