#include <iterator>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>

#include "InputPersonFile.h"
#include "CSVParser.h"

namespace Console
{

bool InputPersonFile::LoadRequestsFromFile()
{
	std::ifstream  MyFile;
	std::string LastPersonName = "";
	std::string NewPersonName = "";
	std::string EmptyStr = "";
	std::string msg;
	Core::Person *MyPerson = nullptr;
	Core::InputRequestList MyRequests;
	Core::InputRequest *MyRequest = nullptr;
	CSVIterator::CSVParser MyParser;

	try
	{
		MyFile = std::ifstream(GetFileName());
	}
	catch (const std::exception&)
	{
		return false;
	}


	while (MyFile >> MyParser)
	{
		// std::cout << MyParser[0] << "|" << MyParser[1] << "|" << MyParser[2] << "|" << MyParser[3] << endl;

		MyRequest = new Core::InputRequest(std::stoi(MyParser[1]), std::stoi(MyParser[2]), std::stoi(MyParser[3]));

		NewPersonName = string(MyParser[0]);

		if (LastPersonName.compare(EmptyStr)==0) {

			MyRequests.push_back(MyRequest);

		} else {

			
			if (LastPersonName.compare(NewPersonName)==0) { // Same Person - Append request

				MyRequests.push_back(MyRequest);

			} else { //New person - Create Request

				// Create Person
				CreatePerson(LastPersonName.c_str(), &MyRequests);

				// Clear Request lits
				MyRequests.clear();

				//Add request to new person
				MyRequests.push_back(MyRequest);
				
			}
		}

		LastPersonName = NewPersonName;
		
	}

	// Create for last Line
	if (MyRequests.size() > 0) {

		CreatePerson(LastPersonName.c_str(), &MyRequests);
	}

	return true;
}

const char *InputPersonFile::GetFileName()
{
	return MyFileName.c_str();
}

void InputPersonFile::SetFileName(const char * sFileName)
{
	MyFileName = sFileName;
}

void InputPersonFile::LoadRequestsManualyInteractive()
{
	char LoadManualy;
	string PersonName;
	char InputRequest;
	Core::InputRequestList Requests;
	Core::InputRequest *Request = nullptr;
	int FloorFrom, FloorTo, Interval;

	std::cout << "Would you like to load Requests manualy? (Y/N)";

	try
	{
		std::cin >> LoadManualy;
	}
	catch (const std::exception&)
	{
		LoadManualy = 'N';
	}

	if (LoadManualy != 'Y') return;


	do
	{
		cout << "\nInform Person name (empty to exit): ";


		try
		{
			//cin >> PersonName;
			getline(cin, PersonName);
			getline(cin, PersonName);

		}
		catch (const std::exception&)
		{
			PersonName = "";
		}

		if (PersonName.compare("")) {

			InputRequest = 'Y';

			Requests.clear();

			do {

				cout << "\nInform Floor From for " << PersonName.c_str() << ": ";

				try
				{
					cin >> FloorFrom;
				}
				catch (const std::exception&)
				{
					FloorFrom = 0;
				}


				cout << "\nInform Floor to for " << PersonName.c_str() << ": ";

				try
				{
					cin >> FloorTo;
				}
				catch (const std::exception&)
				{
					FloorTo = 0;
				}


				cout << "\nInform time befor start request for " << PersonName.c_str() << ": ";

				try
				{
					cin >> Interval;
				}
				catch (const std::exception&)
				{
					Interval = 0;
				}

				if (FloorFrom != FloorTo) {

					Request = new Core::InputRequest(FloorFrom, FloorTo, Interval);

					Requests.push_back(Request);

				}
				cout << "\nRequest added for " << PersonName.c_str() << " to go from floor "
					<< FloorFrom << " to floor " << FloorTo << " waiting " << Interval << " cycles";

				cout << "\nWould you like to add another request for " << PersonName.c_str() << "?(Y/N): ";

				try
				{
					cin >> InputRequest;
				}
				catch (const std::exception&)
				{
					InputRequest = 'N';
				}

			} while (InputRequest == 'Y');

			CreatePerson(PersonName.c_str(), &Requests);

		}

	} while (PersonName.compare(""));

}

void InputPersonFile::LoadRequestsFromFileInteractive()
{
	string msg;
	char LoadFromFile;
	string RequestFile;

	std::cout << "Would you like to load Requests from file? (Y/N)";

	try
	{
		std::cin >> LoadFromFile;
	}
	catch (const std::exception&)
	{
		LoadFromFile = 'N';
	}

	if (LoadFromFile != 'Y') return;


	std::cout << "Set the file name (Default '" << GetFileName() << "'): ";

	try
	{
		//std::cin >> RequestFile;
		getline(cin, RequestFile);
		getline(cin, RequestFile);
	}
	catch (const std::exception&)
	{
		RequestFile = "";
	}

	if (RequestFile.compare(string("")))
		SetFileName(RequestFile.c_str());

	if (!LoadRequestsFromFile()) {

		msg = string("Error loading requests from file") + string(GetFileName());

		LogMessageInternal(Core::MSG_ERROR, msg.c_str());
	}

}

InputPersonFile::InputPersonFile(const char *sName, Core::LogBase *oLogBase, Core::ControlerBase *oControlerBase, Core::FactoryBase *oFactory, const char *sFilename)
:Core::InputPersonBase(sName, oLogBase, oControlerBase, oFactory)
{
	SetFileName(sFilename);
}


void InputPersonFile::CreateRequestsInteractive()
{

	LoadRequestsFromFileInteractive();

	LoadRequestsManualyInteractive();
	
}

}  // namespace Console
