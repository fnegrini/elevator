#include "FactoryStream.h"
#include "PersonInterfaceStream.h"
#include "ElevatorInterfaceStream.h"
#include "ElevatorControlerInterfaceStream.h"
#include "LogStream.h"
#include "InputPersonFile.h"

#include <iostream>
	using namespace std;

const char FILE_NAME[] = "Requests.csv";

namespace Console {



FactoryStream::FactoryStream()
{
}

Core::ElevatorAbstractInterface * FactoryStream::CreateElevatorAbstractInterface(Core::Elevator * oElevator)
{
	return new ElevatorInterfaceStream(oElevator);
}

Core::ElevatorControlerAbstractInterface * FactoryStream::CreateElevatorControlerAbstractInterface(Core::ElevatorControler * oElevatorControler)
{
	return new ElevatorControlerInterfaceStream(oElevatorControler);
}

Core::PersonAbstractInterface * FactoryStream::CreatePersonAbstractInterface(Core::Person * oPerson)
{
	return new PersonInterfaceStream(oPerson);
}

Core::LogBase * FactoryStream::CreateLog()
{
	return new LogStream();
}

Core::InputPersonBase * FactoryStream::CreateInputPerson(const char * sName, Core::LogBase * oLogBase, Core::ControlerBase * oControlerBase)
{
	return new InputPersonFile(sName, oLogBase, oControlerBase, this, FILE_NAME);
}

void FactoryStream::SetupEnvironmentInteractive(Core::ControlerBase * oControler)
{
	int Floors = 10;
	int ElevatorCount = 4;
	int ElevatorIndex, MinFloor, MaxFloor;

	string ControlerName, ElevatorName;

	char SetElevatorRange;

	std::cout << "\nSet the number of floors: ";

	try
	{
		std::cin >> Floors;
	}
	catch (const std::exception&)
	{
		Floors = 10;
	}

	std::cout << "\nSet the number of elevators: ";

	try
	{
		std::cin >> ElevatorCount;
	}
	catch (const std::exception&)
	{
		ElevatorCount = 4;
	}

	std::cout << "\nWould you like to set the range per elevator (Y/N)?: ";

	try
	{
		std::cin >> SetElevatorRange;
	}
	catch (const std::exception&)
	{
		SetElevatorRange = 'N';
	}

	for (ElevatorIndex = 1; ElevatorIndex <= ElevatorCount; ElevatorIndex++) {

		ControlerName = string("Controler ") + string(to_string(ElevatorIndex));
		ElevatorName = string("Elevator ") + string(to_string(ElevatorIndex));

		if (SetElevatorRange == 'Y') {
			cout << "\nSet Min Floor for " << ElevatorName.c_str() << ": ";

			try
			{
				std::cin >> MinFloor;
			}
			catch (const std::exception&)
			{
				MinFloor = 0;
			}

			cout << "\nSet Max Floor for " << ElevatorName.c_str() << ": ";

			try
			{
				std::cin >> MaxFloor;
			}
			catch (const std::exception&)
			{
				MaxFloor = Floors;
			}
		}
		else {

			MinFloor = 0;
			MaxFloor = Floors;
		}

		oControler->CreateControler(ControlerName.c_str(), ElevatorName.c_str(), MinFloor, MaxFloor);
	}

}

Core::ControlerType FactoryStream::GetControlerTypeInteractive()
{
	int Option;

	cout << "\nControler Type available: ";
	cout << "\n1. Coletive UP, Coletive Down";
	cout << "\n2. Coletive UP, Seletive Down";
	cout << "\n3. Seletive UP, Coletive Down";
	cout << "\n4. Seletive UP, Seletive Down";
	cout << "\nChoose the controler type: ";

	cin >> Option;

	switch (Option)
	{
	case 1:
		return Core::CT_CUP_CDOWN;
		break;
	case 2:
		return Core::CT_CUP_SDOWN;
		break;
	case 3:
		return Core::CT_SUP_CDOWN;
		break;
	case 4:
		return Core::CT_SUP_SDOWN;
		break;
	default:
		return Core::CT_STANDARD;
		break;
	}
	
}


}

