#ifndef CONSOLE_ELEVATOR_STREAM_H
#define CONSOLE_ELEVATOR_STREAM_H

#include "../Core/ElevatorAbstractInterface.h"

namespace Console {

class ElevatorInterfaceStream : public Core::ElevatorAbstractInterface
{

public:
	ElevatorInterfaceStream(Core::Elevator *oElevator = nullptr);

	virtual void MovingUp();

	virtual void MovingDown();

	virtual void ArrivedToFloor(int Floor);

	virtual void DoorOpened();

	virtual void DoorClosed();

	virtual void AddPerson(Core::Person *oPerson);
};

}

#endif