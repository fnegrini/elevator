#ifndef CONSOLE_FACTORY_STREAM_H
#define CONSOLE_FACTORY_STREAM_H

#include "../Core/FactoryBase.h"

namespace Console {

class FactoryStream: public Core::FactoryBase
{
public:

	FactoryStream();

	virtual Core::ElevatorAbstractInterface *CreateElevatorAbstractInterface(Core::Elevator *oElevator);

	virtual Core::ElevatorControlerAbstractInterface *CreateElevatorControlerAbstractInterface(Core::ElevatorControler *oElevatorControler);

	virtual Core::PersonAbstractInterface *CreatePersonAbstractInterface(Core::Person *oPerson);

	virtual Core::LogBase *CreateLog();

	virtual Core::InputPersonBase *CreateInputPerson(const char *sName = "", Core::LogBase *oLogBase = nullptr, Core::ControlerBase *oControlerBase = nullptr);

	virtual void SetupEnvironmentInteractive(Core::ControlerBase *oControler);

	virtual Core::ControlerType GetControlerTypeInteractive();
};

}

#endif
