#ifndef CONSOLE_PERSON_INTERFACE_STREAM_H
#define CONSOLE_PERSON_INTERFACE_STREAM_H

#include "../Core/PersonAbstractInterface.h"

namespace Console {


class PersonInterfaceStream : public Core::PersonAbstractInterface
{

public:
	PersonInterfaceStream(Core::Person *oPerson);

	virtual void SimulatingOperation(Core::Request *oRequest);

	virtual void RequestStarted(Core::Request *oRequest);

	virtual void IntoElevator(Core::Request *oRequest, Core::Elevator *oElevator);

	virtual void LeaveElevator(Core::Request *oRequest);

	virtual void RequestFinished(Core::Request *oRequest);
};

}

#endif