#include "ElevatorControlerInterfaceStream.h"


namespace Console {



ElevatorControlerInterfaceStream::ElevatorControlerInterfaceStream(Core::ElevatorControler *oElevatorControler)
:Core::ElevatorControlerAbstractInterface(oElevatorControler)
{
}

void ElevatorControlerInterfaceStream::TripStarted(Core::Trip * oTrip)
{
}

void ElevatorControlerInterfaceStream::RequestCommited(Core::Trip * oTrip, int Floor)
{
}

void ElevatorControlerInterfaceStream::TripFinished(Core::Trip * oTrip)
{
}

}