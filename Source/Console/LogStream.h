#ifndef CONSOLE_LOG_STREAM_H
#define CONSOLE_LOG_STREAM_H

#include "../Core/LogBase.h"

namespace Console
{
class LogStream : public Core::LogBase
{
private:
	std::string FormatOutput(Core::LoggerBase *oLoggerBase, Core::MessageType Type, const char *Message);

protected:
	virtual void LogMessageInternal(Core::LoggerBase *oLoggerBase, Core::MessageType Type, const char *Message);

};

}  // namespace Console
#endif
