#include "CSVParser.h"

#include <iterator>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

namespace Console {


std::string const & CSVIterator::CSVParser::operator[](std::size_t index) const
{
	return MyData[index];
}

std::size_t CSVIterator::CSVParser::size() const
{
	return MyData.size();
}

void CSVIterator::CSVParser::GetNextRow(std::istream & str)
{

	std::string Line;
	std::getline(str, Line);

	std::stringstream   LineStream(Line);
	std::string         Cell;

	MyData.clear();

	while (std::getline(LineStream, Cell, ','))
	{
		MyData.push_back(Cell);
	}

}

std::istream & operator >> (std::istream & str, CSVIterator::CSVParser & data)
{
	data.GetNextRow(str);
	return str;
}

CSVIterator::CSVIterator(std::istream & str)
:MyStream(str.good() ? &str : NULL)
{
	++(*this);
}

CSVIterator::CSVIterator()
: MyStream(NULL)
{

}

CSVIterator & CSVIterator::operator++()
{
	if (MyStream) {

		if (!((*MyStream) >> MyRow)) { 

			MyStream = NULL; 
		} 
	}
	
	return *this;
}

CSVIterator CSVIterator::operator++(int)
{
	CSVIterator    Aux(*this); 

	++(*this); 

	return Aux;

}

CSVIterator::CSVParser const & CSVIterator::operator*() const
{
	return MyRow;
}

CSVIterator::CSVParser const * CSVIterator::operator->() const
{
	return &MyRow;
}

bool CSVIterator::operator==(CSVIterator const & rhs)
{
	return ((this == &rhs) || ((this->MyStream == NULL) && (rhs.MyStream == NULL)));

}

bool CSVIterator::operator!=(CSVIterator const & rhs)
{
	return !((*this) == rhs);
}

}