#ifndef CONSOLE_CSV_PARSER_H
#define CONSOLE_CSV_PARSER_H

#include <vector>

namespace Console {

typedef std::vector<std::string> StringVector;

class CSVIterator
{


public:
	class CSVParser
	{
	private:
		StringVector    MyData;

	public:
		std::string const& operator[](std::size_t index) const;
		std::size_t size() const;
		void GetNextRow(std::istream& str);

	};

	CSVIterator(std::istream& str);

	CSVIterator();

	CSVIterator& operator++();

	CSVIterator operator++(int);

	CSVParser const& operator*() const;

	CSVParser const* operator->()  const;

	bool operator==(CSVIterator const& rhs);

	bool operator!=(CSVIterator const& rhs);
private:

	std::istream* MyStream;

	CSVParser MyRow;
};

// Operator overhide
std::istream& operator >> (std::istream& str, CSVIterator::CSVParser &data);

// Iterator
typedef std::input_iterator_tag IteratorCategory;
typedef CSVIterator::CSVParser ValueType;
typedef std::size_t DifferenceType;
typedef CSVIterator::CSVParser* PCSVParser;
typedef CSVIterator::CSVParser& RCSVParser;

}
#endif