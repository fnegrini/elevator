#include "LogStream.h"
#include "../Core/ComponentBase.h"

#include <iostream>
#include <stdio.h>
#include <Windows.h>
#include <time.h>

namespace Console
{
std::string LogStream::FormatOutput(Core::LoggerBase *oLoggerBase, Core::MessageType Type, const char * Message)
{
	std::string MsgFormated;

	MsgFormated = GetTimeStamp() + std::string(": ")  + std::string(oLoggerBase->GetName()) +
		std::string(" [") + MessageTypeToString(Type) + std::string("]: ") + std::string(Message);

	return MsgFormated;
}

void LogStream::LogMessageInternal(Core::LoggerBase *oLoggerBase, Core::MessageType Type, const char * Message)
{
	std::cout << FormatOutput(oLoggerBase, Type, Message) << "\n";
}

}  // namespace Console
