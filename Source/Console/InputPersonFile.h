#ifndef CONSOLE_INPUT_PERSON_FILE_H
#define CONSOLE_INPUT_PERSON_FILE_H

#include "../Core/InputPersonBase.h"

namespace Console
{
class InputPersonFile : public Core::InputPersonBase
{
private:
	string  MyFileName;

protected:
	
	virtual bool LoadRequestsFromFile();

	virtual const char *GetFileName();

	virtual void SetFileName(const char *sFileName);

	void LoadRequestsManualyInteractive();

	void LoadRequestsFromFileInteractive();

public:
	InputPersonFile(const char *sName = "", Core::LogBase *oLogBase = nullptr, Core::ControlerBase *oControlerBase = nullptr, Core::FactoryBase *oFactory = nullptr, const char *sFilename = "");

	virtual void CreateRequestsInteractive();

};

}  // namespace Console
#endif
