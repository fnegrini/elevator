#include "ElevatorInterfaceStream.h"
#include "../Core/Elevator.h"
#include "../Core/Person.h"

namespace Console {



ElevatorInterfaceStream::ElevatorInterfaceStream(Core::Elevator *oElevator)
:Core::ElevatorAbstractInterface(oElevator)
{
}

void ElevatorInterfaceStream::MovingUp()
{
}

void ElevatorInterfaceStream::MovingDown()
{
}

void ElevatorInterfaceStream::ArrivedToFloor(int Floor)
{
}

void ElevatorInterfaceStream::DoorOpened()
{
}

void ElevatorInterfaceStream::DoorClosed()
{
}

void ElevatorInterfaceStream::AddPerson(Core::Person * oPerson)
{
}

}