#include "PersonInterfaceStream.h"
#include "../Core/Person.h"
#include "../Core/Request.h"
#include "../Core/Elevator.h"

namespace Console {

PersonInterfaceStream::PersonInterfaceStream(Core::Person *oPerson)
:Core::PersonAbstractInterface(oPerson)
{
}

void PersonInterfaceStream::SimulatingOperation(Core::Request * oRequest)
{
}

void PersonInterfaceStream::RequestStarted(Core::Request * oRequest)
{
}

void PersonInterfaceStream::IntoElevator(Core::Request * oRequest, Core::Elevator * oElevator)
{
}

void PersonInterfaceStream::LeaveElevator(Core::Request * oRequest)
{
}

void PersonInterfaceStream::RequestFinished(Core::Request * oRequest)
{
}

}
