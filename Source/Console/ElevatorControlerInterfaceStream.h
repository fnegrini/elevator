#ifndef CONSOLE_ELEVATOR_CONT_STREAM_H
#define CONSOLE_ELEVATOR_CONT_STREAM_H

#include "../Core/ElevatorControlerAbstractInterface.h"

namespace Console {

class ElevatorControlerInterfaceStream : public Core::ElevatorControlerAbstractInterface
{

public:

	ElevatorControlerInterfaceStream(Core::ElevatorControler *oElevatorControler = nullptr);

	virtual void TripStarted(Core::Trip *oTrip);

	virtual void RequestCommited(Core::Trip *oTrip, int Floor);

	virtual void TripFinished(Core::Trip *oTrip);
};

}
#endif