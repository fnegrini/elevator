Step 1: Install QT For windows

Download and install QT for Windows here:
https://www.qt.io/download-open-source/

I have used the 2.0.3 version, witch can be downloaded here:
http://download.qt.io/official_releases/online_installers/qt-unified-windows-x86-online.exe

Step 2: Install QT5package in Visual Studio

In Visual Studio 2015 go to menu: Tools -> Extensions and updates 
Find for QT5Package online. Download and Install
Restart Visual Studio
In Visual Studio 2015 go to menu: QT5 -> QT Options
In the QT Versions click add and set the QT folder installed in Step 1
In my case, the default folder was: C:\Qt\5.6\msvc2015_64\
